#ifndef _PATTERN_TRACKER_H_
#define _PATTERN_TRACKER_H_

#include "mylist.h"

struct pattern_tracker_req_info_t
{
	long long int offset; //this is actually offset difference from the previous request
	struct agios_list_head list;
};

struct access_pattern_t {
	//general information about this pattern
	int reqnb; //number of requests in this pattern
	int read_nb; //number of read requests in this pattern
	int write_nb; //number of write requests in this pattern
	int filenb; //how many different files were accessed during this pattern

	//request list
	struct agios_list_head requests; //while tracking we use the linked list, because it is easier(and we don't know how many requests we'll receive
	struct pattern_tracker_req_info_t *time_series; //after tracking, we translate it to arrays because it will make DTW easier.

	//used by DTW when creating shrunk version of the time series (which we represent by access pattern structs to facilitate implementation
	int original_size; //request number in the time series from which we shrunk this one
	int *aggPtSize; // vector with the number of requests aggregated in each point
};

#define MAXIMUM_FILE_NUMBER 10000 
#define MAX_PATTERN_OFFSET 10485760L //10GB in KB

extern short int agios_is_pattern_tracking;

void add_request_to_pattern(long long int timestamp, long long int offset, long int len, int type, char * file_id);
void pattern_tracker_init();
struct access_pattern_t *get_current_pattern(void);
void free_access_pattern_t(struct access_pattern_t **ap);
void reset_current_pattern(short int first_execution);
void pattern_tracker_exit(void);
#endif

