#include <pthread.h>
#include  <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "pattern_tracker.h"
#include "pattern_matching.h"


int day_carry = 0;
int last_hour = 0;
double select_algorithm_period=1;

int main(int argc, char *argv[])
{
	FILE *fd_in;
	char line[1000];
	char *token_type, *handle, *token_timestamp;
	int type;
	long offset, len;
	double timestamp, window_timestamp;
	int threshold, maximum_difference;

	//get arguments: input_file, threshold, maximum difference
	// and open file
	if(argc < 4)
	{
		printf("Usage: ./%s\tinput_file\tpattern_matching_threshold\tmaximum_pattern_difference\n", argv[0]);
		exit(-1);
	}
	else
	{
		fd_in = fopen(argv[1], "r");
		if(!fd_in)
		{
			printf("Could not open file %s!\n", argv[1]);
			perror("fopen");
			exit(-1);
		}
		threshold = atoi(argv[2]);
		maximum_difference = atoi(argv[3]);
	}

	//start the pattern matching approach
	pattern_matching_init(1, threshold, maximum_difference);

	window_timestamp =0;
	//reads requests from the file and make them to AGIOS
	while(fgets(line, 1000, fd_in))
	{
		        //separate request information to give to AGIOS
		        token_timestamp = strtok(line, "\t");
			timestamp = atol(token_timestamp)/(double)1000000000.0;  //io nodes traces have timestamps in ns, we use seconds here
        		handle = strtok(NULL, "\t");
        		token_type = strtok(NULL, "\t");
			if(strcmp(token_type, "W") == 0)
				type = 0;  //the request types on the traces are inverted
			else if(strcmp(token_type, "R") == 0)
				type = 1;
			else
			{
				printf("I don't know this type! %s\n", token_type);
				exit(-1);
			}
        		offset = atol(strtok(NULL, "\t"));
        		len = atol(strtok(NULL, "\n"));
			//manage timestamps
        		if(window_timestamp == 0)
        			window_timestamp = timestamp;
        		if(timestamp - window_timestamp >= select_algorithm_period)
        		{
				pattern_matching_select_next_algorithm();
        			window_timestamp = timestamp;
        		}

		        add_request_to_pattern((long long int)(timestamp*1000000.0), offset, len, type, handle);
	}
	pattern_matching_select_next_algorithm();
	fclose(fd_in);
	//stop agios and finish
	pattern_matching_exit();
	return 0;
}

