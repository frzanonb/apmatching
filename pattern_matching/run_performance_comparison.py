import subprocess
from numpy import mean

sizes = [10, 100, 1000, 10000, 100000]
reps = 100
results_fastdtw = {}
results_dtw = {}

print("\t\tFastDTW\tDTW")
for size in sizes:
	results_fastdtw[size] = []
	results_dtw[size]=[]
	for r in range(reps):
		run = subprocess.getoutput("./dtw_performance "+str(size)+" 1").split()
		results_fastdtw[size].append(int(run[8]))
		results_dtw[size].append(int(run[10]))

	print(str(size)+"\t"+str(mean(results_fastdtw[size]))+"\t"+str(mean(results_dtw[size])))
	
		
		
		
