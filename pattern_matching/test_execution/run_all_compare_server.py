import os
import subprocess

servers=["10", "15", "16", "17"]
compression = 0
prefix = "/home/fran/traces_nton/multiapp/" #where are the traces?

files={}   #we will have a string of files per operation and per server involved in the test
files["read"] = {}
files["write"] = {}
for op in files:
	for ion in servers:
		files[op][ion]=""
				
#get all the traces
dirs = subprocess.getoutput("ls "+prefix+" | grep NOOP").split('\n')
for dir in dirs:
	for op in files:
		for ion in servers:
			filename = prefix+dir+"/pvfstrace_parasilo-"+ion+".rennes.grid5000.fr."+op
			#we need to check this filename exists
			if not (os.path.isfile(filename)):
				print("PANIC!")
				print(filename)
				exit()
			files[op][ion] += " "+filename

#run the tests
for op in files:
	print("Starting "+op+" tests")
	for i in range(len(servers)):
		print("Starting "+op+" tests with server "+str(i+1))
		os.system("../dtw_all_server "+files[op][servers[i]])
		os.system("mv all_comparisons.txt all_comparisons_"+op+"_1s_comp"+str(compression)+"_server"+str(i+1)+".txt")
