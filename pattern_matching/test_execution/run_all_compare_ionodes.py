import os

ionodes=["2", "3", "4", "5", "6", "7", "8", "9"]
ionodes_nb = [1, 2, 4]
compression = 0
prefix="/home/fran/ioforw_traces/data" #where the traces are

files={}   #we will have a string of files per operation, per number of I/O nodes, and per I/O node involved in the test
files["read"] = {}
files["write"] = {}
for op in files:
	for ion in ionodes_nb:
		files[op][ion]={}
		for i in range(0, ion):
			files[op][ion][i]=""
				
#read the test information to know to what execution each test belongs
arq = open("test_info.csv", "r")
for line in arq:
	if "label" in line:
		continue
	parsed = line.split('\n')[0].split(';')
	ion = int(parsed[1])
	if not (ion in ionodes_nb):
		continue
	for op in files:
		for i in range(0, ion):
			filename = prefix + "/logs-experiment.pattern.matching.4gb.to-"+parsed[0]+"/agios-grisou-"+ionodes[i]+".nancy.grid5000.fr/agios_tracefile.1.out."+op
			#we need to check this filename exists
			if not (os.path.isfile(filename)):
				filename = prefix+"/logs-experiment.pattern.matching.4gb.to-"+parsed[0]+"/agios-log-grisou-"+ionodes[i]+".nancy.grid5000.fr/agios_tracefile.1.out."+op
				if not (os.path.isfile(filename)):
					print("PANIC!")
					print(filename)
					exit()
			files[op][ion][i] += " "+filename
			

arq.close()

#run the tests
for op in files:
	print("Starting "+op+" tests")
	for ion in files[op]:
		print("Starting "+op+" tests with "+str(ion)+" I/O nodes")
		for i in files[op][ion]:
			print("Starting "+op+" tests with "+str(ion)+" I/O nodes, I/O node number "+str(i+1))
			os.system("../dtw_all_ionodes "+files[op][ion][i])
			os.system("mv all_comparisons.txt all_comparisons."+op+"."+str(ion)+"ionodes.comp"+str(compression)+".node"+str(i+1)+".txt")
