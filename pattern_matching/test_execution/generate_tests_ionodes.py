#this script generates a test_list_ionodes.sh file with the list of all commands to run to perform the desired experiments
import os 

########################################################################
#1 - get the names for the trace files that will be used for the tests
########################################################################

ionodes_nb = [1,2,4]
ionodes=["2", "3", "4", "5", "6", "7", "8", "9"]
prefix="/home/fran/ioforw_traces/data/" #where the trace files are

files={}   #we will have a string of files per operation, per number of I/O nodes, and per I/O node involved in the test
files["read"] = {}
files["write"] = {}
for op in files:
	for ion in ionodes_nb:
		files[op][ion]={}
		for i in range(0, ion):
			files[op][ion][i]=""
				
#read the test information to know to what execution each test belongs
arq = open("test_info.csv", "r")
for line in arq:
	if "label" in line:
		continue
	parsed = line.split('\n')[0].split(';')
	ion = int(parsed[1])
	if not (ion in ionodes_nb):
		continue
	for op in files:
		for i in range(0, ion):
			filename = prefix+"/logs-experiment.pattern.matching.4gb.to-"+parsed[0]+"/agios-grisou-"+ionodes[i]+".nancy.grid5000.fr/agios_tracefile.1.out."+op
			#we need to check this filename exists
			if not (os.path.isfile(filename)):
				filename = prefix+"/logs-experiment.pattern.matching.4gb.to-"+parsed[0]+"/agios-log-grisou-"+ionodes[i]+".nancy.grid5000.fr/agios_tracefile.1.out."+op
				if not (os.path.isfile(filename)):
					print("PANIC!")
					print(filename)
					exit()
			if files[op][ion][i] != "":
				files[op][ion][i] += ","
			files[op][ion][i] += filename
arq.close()


#general parameters of the tests
maximum_difference = [40, 45, 50, 55, 60, 65, 70, 75, 80] 
threshold = [95, 96, 97, 98, 99] 
operations = ["read", "write"]
repetitions = 10  #repetitions of this experiment, nothing to do with the repetitions of the experiments that generated the traces

#now create list of tests to run with that script
arq = open("test_list_ionodes.sh", "w")

for op in range(len(operations)):
	for dif in range(len(maximum_difference)):
		for t in range(len(threshold)):
			for ion in ionodes_nb:
				for i in range(0, ion):
					for r in range(0, repetitions):
						#identify this test
						testname = "multiplepatterns_"+operations[op]+"_maxdiff"+str(maximum_difference[dif])+"_threshold"+str(threshold[t])+"_"+str(ion)+"ionodes_node"+str(i)+"_repet"+str(r+1)
						#write it to the list
						arq.write("python multiplepatterns_ionodes.py "+testname+" "+str(threshold[t])+" "+str(maximum_difference[dif])+" "+files[operations[op]][ion][i]+"\n")
arq.close()
