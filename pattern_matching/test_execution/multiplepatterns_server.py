import sys
import os
import random

tracefile_prefix=sys.argv[1] #prefix here goes until before the spatiality

tracefile_sufix=sys.argv[2] #from the repetition until the end of the file name
testname=sys.argv[3]
threshold=sys.argv[4]
maximum_diff=sys.argv[5]
repetitions=sys.argv[6].split(',') #separated by commas, with no blanks
processes=sys.argv[7].split(',') #same as repetitions (regarding format)
spatialities=sys.argv[8].split(',') #same as repetitions (regarding format)
reqsizes=sys.argv[9].split(',') #same as repetitions (regarding format)
filesnb = sys.argv[10].split(',')
prefix="/home/fran"


os.system("date > "+prefix+"/test_trace.txt")

os.system("rm -rf /tmp/agios_pattern_matching.dat")

#we have lists with different parameters 
#create a list with all combinations
tests = []
for s in spatialities:
	for req in reqsizes:
		for r in repetitions:
			if (s == "contig") and (req == "4096reqsX32KB"):
				continue
			for p in processes:
				for f in filesnb:
					if(s == "noncontig") and (f == "nton"):
						continue
					tests.append([s, req, r,p, f])

#run and save output in our test trace
#while we take from the list in random order
random.seed()
while len(tests) > 0:
	#take next random test
	this_test = tests[random.randint(0,len(tests)-1)]
	tests.remove(this_test)
	
	tracefile = tracefile_prefix+this_test[0]+"_"+this_test[1]+"_64procs_32machines_"+this_test[4]+"_app2_"+this_test[0]+"_"+this_test[1]+"_"+str(this_test[3])+"procs_32machines_"+this_test[4]+"_"+str(this_test[2])+tracefile_sufix

	#copy the tracefile here
	os.system("rm -rf "+prefix+"/trace.txt")
	os.system("cp "+tracefile+" "+prefix+"/trace.txt")
	
	#run the code
	os.system("echo "+tracefile+" >> "+prefix+"/test_trace.txt")
	os.system("echo \"../dtw_test_server "+prefix+"/trace.txt "+str(threshold)+" "+str(maximum_diff)+"\" >> "+prefix+"/test_trace.txt")
	os.system("../dtw_test_server "+prefix+"/trace.txt "+str(threshold)+" "+str(maximum_diff)+" >> "+prefix+"/test_trace.txt")
	os.system("echo \"===============================================================================\" >> "+prefix+"/test_trace.txt")
	
os.system("date >> "+prefix+"/test_trace.txt")

#organize results
os.system("mkdir "+prefix+"/tmp_results")
diretorio=prefix+"/tmp_results/"+testname+"/"
os.system("mkdir "+diretorio)
os.system("cp "+prefix+"/test_trace.txt "+diretorio)
os.system("cp /tmp/agios_pattern_matching.dat "+diretorio)
os.system("chmod 777 "+diretorio)
os.system("chmod 777 "+diretorio+"/*")

