import sys
import os
import random

testname=sys.argv[1]
threshold=sys.argv[2]
maximum_diff=sys.argv[3]
tests = sys.argv[4].split(',')
prefix="/home/fran"

os.system("date > "+prefix+"/test_trace.txt")

os.system("rm -rf /tmp/agios_pattern_matching.dat")

#run and save output in our test trace
#while we take from the trace list in random order
random.seed()
while len(tests) > 0:
	#take next random test
	tracefile = tests[random.randint(0,len(tests)-1)]
	tests.remove(tracefile)
	
	#copy the tracefile here
	os.system("rm -rf "+prefix+"/trace.txt")
	os.system("cp "+tracefile+" "+prefix+"/trace.txt")
	
	#run the code
	os.system("echo "+tracefile+" >> "+prefix+"/test_trace.txt")
	os.system("echo \"../dtw_test_ionodes "+prefix+"/trace.txt "+str(threshold)+" "+str(maximum_diff)+"\" >> "+prefix+"/test_trace.txt")
	os.system("../dtw_test_ionodes "+prefix+"/trace.txt "+str(threshold)+" "+str(maximum_diff)+" >> "+prefix+"/test_trace.txt")
	os.system("echo \"===============================================================================\" >> "+prefix+"/test_trace.txt")
	
os.system("date >> "+prefix+"/test_trace.txt")

#organize results
os.system("mkdir "+prefix+"/tmp_results/")
diretorio=prefix+"/tmp_results/"+testname+"/"
os.system("mkdir "+diretorio)
os.system("cp "+prefix+"/test_trace.txt "+diretorio)
os.system("cp /tmp/agios_pattern_matching.dat "+diretorio)
os.system("chmod 777 "+diretorio)
os.system("chmod 777 "+diretorio+"/*")

