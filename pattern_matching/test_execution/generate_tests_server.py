#this script generates a test_list_server.sh file with the list of all commands to run to perform the desired experiments
import os

def list_to_str(lista):
	stri = ""
	for elem in lista:
		stri += str(elem)
		if elem != lista[len(lista)-1]:
			stri+=","
	return stri

#general parameters about the tests we'll execute
servers = [10, 15, 16, 17]
spat = ["contig", "noncontig"]
reqs = ["8reqsX16384KB", "4096reqsX32KB", "32reqsX4096KB"]
operations=["write", "read"]
repetitions = [1, 2, 3, 4, 5, 6] #the repetitions of the experiments that generated the traces, not of this experiment 
processes = [32, 64]
filesnb = ["nto1", "nton"]
maximum_difference = [20, 30, 40, 50, 60, 70, 80, 90] 
threshold = [94,95,96, 97, 98,99] 
reps = 10  #repetitions of this experiment
prefix="/home/fran/traces_nton/multiapp/"  #where the trace files are

spat_str = list_to_str(spat)
reqs_str = list_to_str(reqs)
repetitions_str = list_to_str(repetitions)
processes_str = list_to_str(processes)
filesnb_str = list_to_str(filesnb)

#now create list of tests to run with that script
arq = open("test_list_server.sh", "w")

for op in range(len(operations)):
	for dif in range(len(maximum_difference)):
		for t in range(len(threshold)):
			for d in range(len(servers)):
				for r in range(0,reps):
					#identify this test
					testname = "multiplepatternsmixed_"+operations[op]+"_maxdiff"+str(maximum_difference[dif])+"_threshold"+str(threshold[t])+"_server"+str(d)+"_rep"+str(r+1)
					#write it to the list
					tracefile_prefix=prefix+"NOOP_app1_"
					tracefile_sufix="_pvfstrace/pvfstrace_parasilo-"+str(servers[d])+".rennes.grid5000.fr."+operations[op]
					arq.write("python multiplepatterns_server.py "+tracefile_prefix+" "+tracefile_sufix+" "+testname+" "+str(threshold[t])+" "+str(maximum_difference[dif])+" "+repetitions_str+" "+processes_str+" "+spat_str+" "+reqs_str+" "+filesnb_str+"\n")
arq.close()
