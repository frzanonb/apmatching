#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "pattern_tracker.h"
#include "DTW.h"
#include "mylist.h"
#include "pattern_matching.h"


#define agios_print(f, a...) 					printf("AGIOS: " f "\n", ## a)


#ifdef PATTERN_MATCHING_DEBUG
#define PRINT_FUNCTION_NAME agios_print("%s\n", __PRETTY_FUNCTION__)
#define PRINT_FUNCTION_EXIT agios_print("%s exited\n", __PRETTY_FUNCTION__)
#define debug(f, a...) agios_print("%s(): " f "\n", __PRETTY_FUNCTION__, ## a)
#else
#define PRINT_FUNCTION_NAME (void)(0)
#define PRINT_FUNCTION_EXIT (void)(0)
#define debug(f, a...) (void)(0)
#endif


#define config_pattern_filename "/tmp/agios_pattern_matching.dat"

struct PM_pattern_t *previous_pattern = NULL;
struct PM_pattern_t *predicted_next_pattern=NULL;
AGIOS_LIST_HEAD(all_observed_patterns);
short int first_performance_measurement;
long int access_pattern_count=0;
int current_selection;
long long int max_dtw_result=1;
short int update_max_dtw_result = 1;
int call_count=0;
long int fastdtw_call_count=0;
long long int fastdtw_size=0;
long long int call_size=0;

int config_minimum_pattern_size=1;
int config_pattern_matching_threshold=80;
int config_maximum_pattern_difference=10;

void agios_gettime(struct timespec * timev)
{
	clock_gettime(CLOCK_MONOTONIC, timev);
}
long int get_timespec2llu(struct timespec t)
{
	return (t.tv_sec*1000000000L + t.tv_nsec);
}


//cleanup a PM_pattern_t structure
void free_PM_pattern_t(struct PM_pattern_t **pattern)
{
	if((*pattern))
	{
		//free the access pattern description structure
		free_access_pattern_t(&((*pattern)->description));
		//free the probability network
		if(!agios_list_empty(&((*pattern)->next_patterns)))
		{
			struct next_patterns_element_t *tmp, *aux=NULL;
			agios_list_for_each_entry(tmp, &(*pattern)->next_patterns, list)
			{
				if(aux)
				{
					agios_list_del(&aux->list);
					free(aux);
				}
				aux = tmp;
			}
			if(aux)
			{
				agios_list_del(&aux->list);
				free(aux);
			}
		}

		free(*pattern);
	}
}

//read information about an access pattern from the file, and its performance measurements (but not the probability network yet)
struct PM_pattern_t *read_access_pattern_from_file(FILE *fd)
{
	size_t error;
	int i;
	//allocate memory
	//struct scheduler_info_t *sched_info;
	struct PM_pattern_t *ret = malloc(sizeof(struct PM_pattern_t));
	if(!ret)
		return ret;
	ret->description = malloc(sizeof(struct access_pattern_t));
	if(!ret->description)
	{
		free(ret);
		return NULL;
	}

	//description of the access pattern first
	error = 0;
	error += fread(&(ret->description->reqnb), sizeof(int), 1, fd);
	error += fread(&(ret->description->read_nb), sizeof(int), 1, fd);
	error += fread(&(ret->description->write_nb), sizeof(int), 1, fd);
	error += fread(&(ret->description->filenb), sizeof(int), 1, fd);
	if(error != 4)
	{
		free_PM_pattern_t(&ret);
		return NULL;
	}
	init_agios_list_head(&ret->description->requests);
	//the time series
	ret->description->time_series = malloc(sizeof(struct pattern_tracker_req_info_t)*((ret->description->reqnb)+1));
	if(!ret->description->time_series)
	{
		free_PM_pattern_t(&ret);
		return NULL;
	}
	error = 0;
	for(i =0; i<ret->description->reqnb; i++)
	{
		error += fread(&(ret->description->time_series[i].offset), sizeof(long long int), 1, fd);
		init_agios_list_head(&(ret->description->time_series[i].list));
	}
	if(error != ret->description->reqnb)
	{
		free_PM_pattern_t(&ret);
		return NULL;
	}
	ret->description->original_size = ret->description->reqnb;
	ret->description->aggPtSize = NULL;

	//initialize the rest of the structure to fill probability network later
	init_agios_list_head(&(ret->next_patterns));
	ret->all_counters = 0;

	return ret;
}

void read_pattern_matching_file()
{
	FILE *fd;
	size_t ret, this_ret;
	int pat_nb, next_pat;
	struct PM_pattern_t **patterns;
	struct PM_pattern_t *new;
	struct next_patterns_element_t *tmp;
	int i;

	PRINT_FUNCTION_NAME;

	//initialize the list of patterns
	init_agios_list_head(&all_observed_patterns);

	//open file
	fd = fopen(config_pattern_filename, "r");
	if(!fd)
	{
		agios_print("Could not open pattern matching file %s\n", config_pattern_filename);
		return;
	}

	//read the number of access patterns
	ret = fread(&access_pattern_count, sizeof(long int), 1, fd);
	if(ret != 1)
	{
		agios_print("PANIC! Could not read from access pattern file %s\n", config_pattern_filename);
		fclose(fd);
		return;
	}
	//we'll allocate a vector to help us while reading
	patterns = malloc(sizeof(struct PM_pattern_t *)*(access_pattern_count+1));
	if(!patterns)
	{
		agios_print("PANIC! Could not allocate memory for access pattern tracking\n");
		fclose(fd);
		return;
	}

	//first we read the information on the known access patterns
	for(i=0; i< access_pattern_count; i++)
	{
		//get it
		new = read_access_pattern_from_file(fd);
		new->id = i;
		//store it in our vector
		patterns[i] = new;
		//put it in the list
		agios_list_add_tail(&new->list, &all_observed_patterns);
	}

	//then we read the probability network
	ret = 0;
	agios_list_for_each_entry(new, &all_observed_patterns, list)
	{
		//we store a counter of future patterns
		ret += fread(&pat_nb, sizeof(int), (size_t)1, fd);
		this_ret = 0;
		for(i = 0; i< pat_nb; i++)
		{
			//we build a new next pattern data structure
			tmp = malloc(sizeof(struct next_patterns_element_t));
			if(!tmp)
			{
				agios_print("PANIC! Could not allocate memory for access pattern tracking\n");
				fclose(fd);
				free(patterns);
				return;
			}
			//get the identifier of the next pattern and store it
			this_ret += fread(&next_pat, sizeof(int), (size_t)1, fd);
			tmp->pattern = patterns[next_pat];
			//get other relevant information
			this_ret += fread(&tmp->counter, sizeof(int), 1, fd);
			new->all_counters += tmp->counter;
			this_ret += fread(&tmp->probability, sizeof(int),  1, fd);
			//store this information for this access pattern
			agios_list_add_tail(&tmp->list, &new->next_patterns);
		}
		if(this_ret != pat_nb*3)
		{
			agios_print("PANIC! Could not read pattern matching probability network information\n");
			fclose(fd);
			free(patterns);
			return;
		}
	} //end agios_list_for_each_entry
	if(ret != access_pattern_count)
	{
		agios_print("PANIC! Could not read pattern matching probability network information\n");
		fclose(fd);
		free(patterns);
		return;
	}

	//last information: maximum DTW result observed so far (we use it to calculate % difference between patterns)
	ret = fread(&max_dtw_result, sizeof(long long int),  (size_t)1, fd);
	if(ret != 1)
	{
		agios_print("Error! Could not read maximum dtw result from pattern matching file\n");
	}

	//close file
	fclose(fd);
	//free memory
	free(patterns);

	debug("Read %ld patterns from the pattern matching file, the maximum DTW score so far is %lld\n", access_pattern_count, max_dtw_result);
}

int pattern_matching_init(int minimum_size, int threshold, int maximum_difference)
{
	config_minimum_pattern_size=minimum_size;
	config_pattern_matching_threshold=threshold;
	config_maximum_pattern_difference=maximum_difference;
	debug("Running PATTERN MATCHING with minimum pattern size %d, threshold %d, and maximum difference %d\n", config_minimum_pattern_size, config_pattern_matching_threshold, config_maximum_pattern_difference);

	first_performance_measurement = 1;
	//start the pattern tracker
	pattern_tracker_init();

	//read the pattern matching file we keep between executions so we are always smart
	read_pattern_matching_file();
//	max_dtw_result = 353844208L;  //ionode traces, 1 i/o node, compression of 100
//	max_dtw_result = 187834869L; //ionode traces, 2 i/o nodes, compression of 100
//	max_dtw_result = 175986050L; //ionode traces, 4 i/o nodes, compression of 100
//	max_dtw_result = 3342751260LL; //ionode traces, 1 i/o node, compression of 10
//	max_dtw_result = 1785263335LL; //ionode traces, 2 i/o nodes, compression of 10
//	max_dtw_result = 1678123622LL; //ionode traces, 4 i/o nodes, compression of 10
//	max_dtw_result = 82578613984LL; //server traces, compression of 0
	max_dtw_result = 8467267360LL; //server traces, compression of 10
//	max_dtw_result = 1742214033LL; //server traces, compression of 100
//	max_dtw_result = 177852186LL; //server traces, compression of 1000
	update_max_dtw_result = 0; //IMPORTANT if we don't set max_dtw_result here, leave this at 1 so we learn it during the execution

	current_selection = 1;

	return 1;
}

int get_percentage_difference(int valueA, int valueB)
{
	int diff = 100;
	if(valueA == valueB)
		return 0;
	if(valueA > valueB)
	{
		if(valueA > 0)
			diff = ((valueA - valueB)*100)/valueA;
	}
	else
	{
		if(valueB > 0)
			diff = ((valueB - valueA)*100)/valueB;
	}
	return diff;
}

//apply some heuristics to decide if two access patterns are close enough to compare their time series
short int compatible_pattern(struct access_pattern_t *A, struct access_pattern_t *B)
{
	//let's see the difference in the total number of requests
	if(get_percentage_difference(A->reqnb, B->reqnb) > config_maximum_pattern_difference)
		return 0;
	//let's see the difference in the number of reads
	if(get_percentage_difference(A->read_nb, B->read_nb) > config_maximum_pattern_difference)
		return 0;
	//let's see the difference in the number of writes
	if(get_percentage_difference(A->write_nb, B->write_nb) > config_maximum_pattern_difference)
		return 0;
	//let's see the difference in the number of accessed files
	if(get_percentage_difference(A->filenb, B->filenb) > config_maximum_pattern_difference)
		return 0;
	//ok, they passed all our tests, so we say they are compatible
	return 1;
}

//apply FastDTW to get the distance between the two time series (from the two access patterns), and then use the maximum distance ever observed (possibly updating it) to give it a similarity degree between 0 and 100%.
int apply_DTW(struct access_pattern_t *A, struct access_pattern_t *B)
{
	long long int dtw_result = FastDTW(A, B); 
	fastdtw_call_count++;
	fastdtw_size += max(A->reqnb, B->reqnb);
	if(dtw_result > max_dtw_result)
	{
		if(update_max_dtw_result)
			max_dtw_result = dtw_result; 
		return 0;
	}
	else if(dtw_result == 0) //best possible score
		return 100;
	else
		return ((max_dtw_result - dtw_result)*(long long int)100)/max_dtw_result; //dtw_result is a score that is higher when patterns are more different, but with this formula we get a score that is higher when patterns are more similar
}

//look for an access pattern in the list of patterns we know
//return NULL if we can't find a match
struct PM_pattern_t *match_seen_pattern(struct access_pattern_t *pattern)
{
	struct PM_pattern_t *tmp, *ret = NULL;
	int best_result = 0.0;
	int this_result;

	PRINT_FUNCTION_NAME;

	agios_list_for_each_entry(tmp, &all_observed_patterns, list)
	{
		//compare the patterns
	 	if(compatible_pattern(tmp->description, pattern)) //first we apply some heuristics so we won't compare all patterns
		{
			this_result = apply_DTW(tmp->description, pattern);
			//among the similar ones, we get the most similar one
			if((this_result >= config_pattern_matching_threshold) && (this_result > best_result))
			{
				best_result = this_result;
				ret = tmp;
			}
		}
	}
	return ret;
}
//store a new access pattern in the list of known access patterns
//returns a pointer to it
struct PM_pattern_t *store_new_access_pattern(struct access_pattern_t *pattern)
{
	//allocate new data structure
	struct PM_pattern_t *ret = malloc(sizeof(struct PM_pattern_t));
	if(!ret)
	{
		agios_print("PANIC! Could not allocate memory for pattern matching!\n");
		return NULL;
	}
	//initialize the data structure
	ret->description = pattern;
	init_agios_list_head(&ret->performance);
	init_agios_list_head(&ret->next_patterns);
	ret->all_counters = 0;
	//store it
	agios_list_add_tail(&ret->list, &all_observed_patterns);
	ret->id = (int)access_pattern_count;
	access_pattern_count++;
	return ret;
}

void update_probability_network(struct PM_pattern_t *new_pattern)
{
	struct next_patterns_element_t *tmp;
	short int found=0;

	//look for this pattern in the next patterns of the previous pattern
	if(!agios_list_empty(&(previous_pattern->next_patterns)))
	{
		agios_list_for_each_entry(tmp, &(previous_pattern->next_patterns), list)
		{
			if(tmp->pattern == new_pattern)
			{
				found =1;
				break;
			}
		}
	}
	//if we did not find it, we need to add a new data structure
	if(!found)
	{
		tmp = malloc(sizeof(struct next_patterns_element_t));
		if(!tmp)
		{
			agios_print("PANIC! Could not allocate memory for pattern matching\n");
			return;
		}
		tmp->counter = 0;
		tmp->pattern = new_pattern;
		agios_list_add_tail(&tmp->list, &previous_pattern->next_patterns);
	}
	//we count this occurrence now
	tmp->counter++;
	previous_pattern->all_counters++; //we use a lazy approach to update probabilities (we only calculate them when writing the file or making a decision)
}
//look at the probability network from previous_pattern and returns the most likely pattern
struct PM_pattern_t *predict_next_pattern(void)
{
	//we recalculate probabilities and get the highest one
	int best_prob = 0;
	struct next_patterns_element_t *tmp;
	struct PM_pattern_t *ret=NULL;
	if(!agios_list_empty(&(previous_pattern->next_patterns)))
	{
		agios_list_for_each_entry(tmp, &previous_pattern->next_patterns, list)
		{
			tmp->probability = (tmp->counter*100)/previous_pattern->all_counters;
			if(tmp->probability == 0)
				tmp->probability = 1;
			if(tmp->probability > best_prob)
			{
				best_prob = tmp->probability;
				ret = tmp->pattern;
			}
		}
	}
	return ret;
}

int pattern_matching_select_next_algorithm(void)
{
	struct access_pattern_t *seen_pattern;
	struct PM_pattern_t *matched_pattern=NULL;

	PRINT_FUNCTION_NAME;


	//get the most recently tracked access pattern from the pattern tracker module
	seen_pattern = get_current_pattern();

	call_count++;
	call_size += seen_pattern->reqnb;

	debug("Adding a pattern of %d requests, %d reads and %d writes with %d files\n", seen_pattern->reqnb, seen_pattern->read_nb, seen_pattern->write_nb, seen_pattern->filenb);

	//look for the recently observed pattern in our list of known patterns
	if(seen_pattern->reqnb >= config_minimum_pattern_size) //we ignore patterns that are too small
	{
		matched_pattern = match_seen_pattern(seen_pattern);
		if(matched_pattern) //we don't need the seen_pattern structure anymore, we can free it
		{
			debug("Found a match with id %d\n", matched_pattern->id);
			free_access_pattern_t(&seen_pattern);
			//see if our prediction was right
			if(predicted_next_pattern)
			{
				debug("Let's compare this pattern to the one we've predicted (%d). Are they the same? %d. Are they compatible? %d. DTW score: %llu  (because max is %llu)\n", predicted_next_pattern->id, predicted_next_pattern->id == matched_pattern->id, compatible_pattern(predicted_next_pattern->description, matched_pattern->description), FastDTW(predicted_next_pattern->description, matched_pattern->description), max_dtw_result);
			}
		}
		else	//if we did not find the pattern, we'll include it as a new one
		{
			matched_pattern = store_new_access_pattern(seen_pattern);
			debug("We did not find a match, stored a new pattern with id %d\n", matched_pattern->id);
		}
	}
	else //the pattern is too short, we'll drop it
	{
		debug("We'll ignore this access pattern because it is too short reqnb is %d, readnb is %d, writenb is %d\n", seen_pattern->reqnb, seen_pattern->read_nb, seen_pattern->write_nb);
		free_access_pattern_t(&seen_pattern);
	}

	//we link the previous pattern to this one we've just detected
	if(matched_pattern)
	{
		if(previous_pattern)
			update_probability_network(matched_pattern);
		previous_pattern = matched_pattern;
		//now we can try to predict the next pattern we'll see
		predicted_next_pattern = predict_next_pattern();
		if(predicted_next_pattern)
		{
			if(predicted_next_pattern->id == previous_pattern->id)
				debug("We predict to continue with the same pattern\n");
		}
	}
	PRINT_FUNCTION_EXIT;
	return current_selection; //if we are running static, we won't ever change the scheduling algorithm, all we do is recognize the pattern
}
//writes information about an access pattern to the file (including performance observed with different scheduling algorithms, NOT including the probability network)
//returns 1 on success, 0 otherwise
int write_access_pattern_to_file(struct PM_pattern_t *pattern, FILE *fd)
{
	size_t error=0;
	int i;

	//description of the access pattern
	error+= fwrite(&(pattern->description->reqnb), sizeof(int), 1, fd);
	error+= fwrite(&(pattern->description->read_nb), sizeof(int), 1, fd);
	error+= fwrite(&(pattern->description->write_nb), sizeof(int), 1, fd);
	error+= fwrite(&(pattern->description->filenb), sizeof(int), 1, fd);
	if(error != 4)
		return 0;
	//the time series
	error = 0;
	for(i = 0; i < pattern->description->reqnb; i++)
	{
		error += fwrite(&(pattern->description->time_series[i].offset), sizeof(long long int), 1, fd);
	}
	if(error != pattern->description->reqnb)
		return 0;
	return 1;
}
//returns the number of future patters (the size of the provided list)
int get_next_pattern_number(struct agios_list_head *chain)
{
	int ret = 0;
	struct next_patterns_element_t *tmp;
	if(!agios_list_empty(chain))
	{
		agios_list_for_each_entry(tmp, chain, list)
		{
			ret++;
		}
	}
	return ret;
}
void write_pattern_matching_file(void)
{
	FILE *fd;
	size_t ret, error, this_error;
	int pat_count;
	struct PM_pattern_t *tmp;
	struct next_patterns_element_t *next;
	long long int call_avg, fastdtw_call_avg;

	PRINT_FUNCTION_NAME;

	if(call_count > 0)
		call_avg = call_size/call_count;
	else
		call_avg = 0;
	if(fastdtw_call_count > 0)
		fastdtw_call_avg = fastdtw_size/fastdtw_call_count;
	else
		fastdtw_call_avg = 0;
	debug("We'll write the pattern_matching file. We have %ld patterns (from %d calls in this execution, average size %lld), max DTW score is %lld. We've called FastDTW %ld times to patterns of average size %lld\n", access_pattern_count, call_count, call_avg, max_dtw_result, fastdtw_call_count, fastdtw_call_avg);

	fd = fopen(config_pattern_filename, "w");
	if(!fd)
	{
		agios_print("Could not open pattern matching file %s\n", config_pattern_filename);
		return;
	}
	else
	{
		//write the number of access patterns
		ret = fwrite(&access_pattern_count, sizeof(long int), 1, fd);
		if(!ret)
		{
			agios_print("PANIC! Could not write to pattern matching file\n");
			fclose(fd);
			return;
		}
		//write basic information on the access patterns (general information, the time series and performance observed with scheduling algorithms)
		agios_list_for_each_entry(tmp, &all_observed_patterns, list)
		{
			ret = write_access_pattern_to_file(tmp, fd);
			if(ret != 1)
			{
				agios_print("PANIC! Could not write to pattern matching file!\n");
				fclose(fd);
				break;
			}
		}
		if(ret != 1)
			return;
		//write the probability network, we'll write something about every pattern we know
		error = 0;
		agios_list_for_each_entry(tmp, &all_observed_patterns, list)
		{
			//write the number of future patterns we know for this one
			pat_count = get_next_pattern_number(&tmp->next_patterns);
			error += fwrite(&pat_count, sizeof(int), 1, fd);
			//write the future patterns and associated information
			this_error = 0;
			agios_list_for_each_entry(next, &tmp->next_patterns, list)
			{
				this_error += fwrite(&next->pattern->id, sizeof(int), 1, fd);
				this_error += fwrite(&next->counter, sizeof(int), 1, fd);
				this_error += fwrite(&next->probability, sizeof(int), 1, fd);
			}
			if(this_error != pat_count*3)
			{
				agios_print("PANIC! Could not write probability network to pattern matching file\n");
				fclose(fd);
				return;
			}
		}
		if(error != access_pattern_count)
		{
			agios_print("PANIC! Could not write probability network to pattern matching file\n");
			fclose(fd);
			return;
		}

		//last information for the pattern matching file: the maximum dtw difference observed so far
		error = fwrite(&max_dtw_result, sizeof(long long int), 1, fd);
		if(error != 1)
			agios_print("PANIC! Could not write maximum dtw result to pattern matching file\n");
		fclose(fd);
	}
}
void pattern_matching_exit(void)
{
	struct PM_pattern_t *tmp, *aux=NULL;

	//write file with what we have learned in this execution
	write_pattern_matching_file();
	//cleanup data structures pre-allocated for DTW
    pattern_tracker_exit();
	//cleanup
	agios_list_for_each_entry(tmp, &all_observed_patterns, list)
	{
		if(aux)
		{
			agios_list_del(&aux->list);
			free_PM_pattern_t(&aux);
		}
		aux = tmp;
	}
	if(aux)
	{
		agios_list_del(&aux->list);
		free_PM_pattern_t(&aux);
	}
}
