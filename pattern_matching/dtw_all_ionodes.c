#include <pthread.h>
#include  <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include <limits.h>
#include "pattern_tracker.h"
#include "DTW.h"


int day_carry = 0;
int last_hour = 0;
double select_algorithm_period=1;

#define PATTERN_NB 20000
void cleanup(struct access_pattern_t ***patterns, int **files, long long int *** scores, FILE *fd_in);

void save_pattern(struct access_pattern_t *new, struct access_pattern_t **pats, int *i, int *fi, int index, long long int *** scores, FILE *fd_in)
{
    pats[*i] = new;
    fi[*i] = index;
    (*i)++;
    if((*i) >= PATTERN_NB)
    {
        printf("PANIC! We found more patterns than what we had allocated memory for\n");
	cleanup(&pats, &fi, scores, fd_in);
	exit(-1);
    }
}

void cleanup(struct access_pattern_t ***patterns, int **files, long long int *** scores, FILE *fd_in)
{
	int i;

    if(*patterns)
    {
	for(i=0; i < PATTERN_NB; i++)
	{
		if((*patterns)[i])
			free_access_pattern_t(&((*patterns)[i]));
		else
			break;
	}
        free(*patterns);
    }
    if(*files)
        free(*files);
    if(*scores)
    {
        for(i=0; i< PATTERN_NB; i++)
        {
            if((*scores)[i])
                free((*scores)[i]);
        }
        free(*scores);
    }
    if(fd_in)
        fclose(fd_in);
}

int main(int argc, char *argv[])
{
	FILE *fd_in=NULL;
    FILE *fd_out=NULL;
	char line[1000];
	char *token_handle, *token_type, *token_offset, *token_len, *token, *handle, *token_timestamp;
	int type;
	long offset, len;
	int hour, minute;
	double seconds, timestamp, last_timestamp, window_timestamp;
    struct access_pattern_t **patterns=NULL;
    long long int **scores=NULL;
long long int max_distance = 0;
    int *files=NULL;
    int i, j, pat;
	long long int ret;
	


	if(argc < 2)
	{
		printf("Usage: ./%s\tinput_file\n", argv[0]);
		exit(-1);
	}
	else
	{
        //allocate memory for patterns and comparisons
        patterns = malloc(sizeof(struct access_pattern_t *)*PATTERN_NB);
        files = malloc(sizeof(int)*PATTERN_NB); //in this list we keep the index of argv where we'll find the file name from where the pattern was read
        scores = malloc(sizeof(long long int *)*PATTERN_NB);
        if((!patterns) || (!scores) || (!files))
        {
            printf("PANIC! Could not allocate memory to keep patterns");
		cleanup(&patterns, &files, &scores, fd_in);
		exit(-1);
        }
        for(i=0; i< PATTERN_NB; i++)
        {
            scores[i] = malloc(sizeof(long long int)*PATTERN_NB);
            if(!scores[i])
            {
                printf("PANIC! Could not allocate memory to keep patterns");
		cleanup(&patterns, &files, &scores, fd_in);
		exit(-1);
            }
            for(j = 0; j < PATTERN_NB; j++)
            {
                scores[i][j] = 0;
            }
        }

        //initialize the pattern tracker (the module that will build the patterns)
        pattern_tracker_init();

        //read all files
        pat = 0;
        printf("We'll read from %d files\n", argc-1);
        for(i = 1; i< argc; i++)
        {
            printf("Reading from %s\n", argv[i]);
            //open the file
    		fd_in = fopen(argv[i], "r");
    		if(!fd_in)
	    	{
		    	printf("Could not open file %s!\n", argv[i]);
    			perror("fopen");
			cleanup(&patterns, &files, &scores, fd_in);
			exit(-1);
		    }

        	//reads requests from the file and add them to patterns
            window_timestamp =0;
        	while(fgets(line, 1000, fd_in))
        	{
		        //separate request information to give to AGIOS
		        token_timestamp = strtok(line, "\t");
			timestamp = atol(token_timestamp)/(double)1000000000.0;  //io nodes traces have timestamps in ns, we use seconds here
        		handle = strtok(NULL, "\t");
        		token_type = strtok(NULL, "\t");
			if(strcmp(token_type, "W") == 0)
				type = 0;  //the request types on the traces are inverted
			else if(strcmp(token_type, "R") == 0)
				type = 1;
			else
			{
				printf("I don't know this type! %s\n", token_type);
				exit(-1);
			}
        		offset = atol(strtok(NULL, "\t"));
        		len = atol(strtok(NULL, "\n"));
			//manage timestamps
        		if(window_timestamp == 0)
        			window_timestamp = timestamp;
        		if(timestamp - window_timestamp >= select_algorithm_period)
        		{
                    //we have a complete pattern, we'll save it
                    save_pattern(get_current_pattern(), patterns, &pat, files, i, &scores, fd_in);
        			window_timestamp = timestamp;
        		}

		        add_request_to_pattern((long long int)(timestamp*1000000.0), offset, len, type, handle);
	        }
            save_pattern(get_current_pattern(), patterns, &pat, files, i, &scores, fd_in);

            //close the file
            fclose(fd_in);
            fd_in = NULL;
        }
        printf("We've read %d patterns from %d files!\n", pat, argc-1);

        //now we'll compare all patterns among themselves (and write results to a file because it is supposed to take a long time and I dont want to redo it)
	printf("Creating output file\n");
        fd_out = fopen("all_comparisons.txt", "w");
        if(!fd_out)
        {
            printf("PANIC! Could not open output file!\n");
            perror("fopen");
	    cleanup(&patterns, &files, &scores, fd_in);
	    exit(-1);
        }
        fprintf(fd_out, "%d patterns\n", pat);
	
        for(i=0; i< pat; i++)
        {
            fprintf(fd_out, "%s\t%d\n", argv[files[i]], i);
	    fprintf(stderr,"Comparing %d to all\n", i);
            for(j=i+1; j < pat; j++)
            {
                ret = FastDTW(patterns[i], patterns[j]);
		scores[i][j] = ret;
		if(ret > max_distance)
			max_distance = ret;
                fprintf(fd_out, "%d\t%d\t%lld\n", i, j, scores[i][j]);
		assert(ret >= 0);
            }
        }
	fprintf(stdout, "%lld\n", max_distance);

        fclose(fd_out);

    }

	    cleanup(&patterns, &files, &scores, fd_in);

	//stop agios and finish
    pattern_tracker_exit();
	return 0;
}

