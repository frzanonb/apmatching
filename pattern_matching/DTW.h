/*
 * Copyright (c) 2004 Stan Salvador
 * stansalvador@hotmail.com

 The MIT License (MIT)

Copyright (c) 2004 Stan Salvador

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 *  Translated from JAVA to C++ by Ramon Nou - Barcelona Supercomputing Center - IOLanes EU Project
 * Translated to C and adapted by Francieli Zanon Boito
 */

/* File:	DTW.h
 * Created: 	November 2016 
 * License:	GPL version 3
 * Author:
 *		Francieli Zanon Boito <francielizanon (at) gmail.com>
 *
 * Description:
 *		This file is part of the AGIOS I/O Scheduling tool.
 *		It contains the fastDTW method implementation to compare access patterns	
 *
 * Contributors:
 *		Federal University of Rio Grande do Sul (UFRGS)
 *		INRIA France
 *	
 *		This program is distributed in the hope that it will be useful,
 * 		but WITHOUT ANY WARRANTY; without even the implied warranty of
 * 		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */
#ifndef _DTW_H_
#define _DTW_H_

#include "mylist.h"
#include "pattern_tracker.h"

struct DTW_point {
	int i;
	int j;
};

struct time_warp_info_t {
	//distance
	long long int distance;
	//path that results in this distance
	struct DTW_point *points;
	int index;
	int max;
};

struct DTW_range {
	int min;
	int max;
};

struct search_window_t {
	struct DTW_range *ranges;
	int maxJ;
	int maxI;
	int size;
};

struct search_window_iterator_t {
	struct DTW_point *points;
	int size;
}; 

struct constrainedDTW_cost_t {
	long long int *cellValues;
	int *colOffsets;
	int colOffsets_size;
};

struct dtw_data_structures_t {
	struct time_warp_info_t *TWinfo;
	int max_timewarppath_len;
	struct search_window_t *SearchWindow;
	int SearchWindow_len;
	struct constrainedDTW_cost_t *CDTW_cost;
	int max_constrainedDTW_cost_columns;
	int max_constrainedDTW_cost_points;
	long long int **cMatrix;
	int costMatrix_sizeI;
	int costMatrix_sizeJ;
	struct search_window_iterator_t *SearchWindowIterator;
	int SearchWindowIterator_len;
};

long long int DTW(struct access_pattern_t *tsI, struct access_pattern_t *tsJ);

long long int FastDTW(struct access_pattern_t *A, struct access_pattern_t *B);


long long int min(long long int value1, long long int value2);
long long int max(long long int value1, long long int value2);
long long int positive_sum(long long int value1, long long int value2);
#endif
