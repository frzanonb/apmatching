#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <time.h>
#include <limits.h>
#include <assert.h>

#include "mylist.h"
#include "pattern_tracker.h"

#define agios_print(f, a...) 					printf("AGIOS: " f "\n", ## a)

#define agios_config_pattern_compression_factor 10
// 0 is average absolute offset distance
// 1 is minimum absolute value
// 2 is maximum absolute value
// 3 is random between minimum or maximum
#define agios_config_pattern_compression_method 0

//to access the tracked_pattern list
pthread_mutex_t pattern_tracker_lock = PTHREAD_MUTEX_INITIALIZER;

//current access pattern
struct access_pattern_t *current_pattern;
struct agios_list_head bucket;
int bucket_length=0;
long long int last_offset=0;
int last_accessed_file = -1;
//files being accessed right now
char * file_ids[MAXIMUM_FILE_NUMBER];

//are we pattern tracking?
short int agios_is_pattern_tracking = 0;

void free_bucket();
long long int summarize_bucket();

//adds information about a new request to the tracked pattern
//offset and len arrive in bytes, we change to KB internally
void add_request_to_pattern(long long int timestamp, long long int offset, long int len, int type, char *file_id)
{
	if(agios_is_pattern_tracking) //so if the flag is not set (only the pattern matching algorithm sets it), this function will do nothing
	{
		int i;
		long long int calculated_offset;

		pthread_mutex_lock(&pattern_tracker_lock);


		//register currently accessed file
		for(i = 0; i< MAXIMUM_FILE_NUMBER; i++)
		{
			if(!file_ids[i])
				break;  //we finished going through all registered files and did not find this one
			if(strcmp(file_ids[i], file_id) == 0)
				break;
		}
		assert(i < MAXIMUM_FILE_NUMBER);
		if(!file_ids[i]) //we need to add this file
		{
			current_pattern->filenb++;
			file_ids[i] = malloc((strlen(file_id)+1)*sizeof(char));
			if(!file_ids[i])
				agios_print("PANIC! No memory for pattern tracker!");
			strcpy(file_ids[i], file_id);
		}
		//i is the index of the file accessed by this request
		//allocate structure and fill it
		struct pattern_tracker_req_info_t *new = malloc(sizeof(struct pattern_tracker_req_info_t));
		if(!new)
		{
			agios_print("PANIC! No memory for pattern tracker!");
			return;
		}
		init_agios_list_head(&new->list);
		calculated_offset = (offset)/1024; //we keep offsets in KB
		//see if this is the same file than the one accessed last
		if((i == last_accessed_file) || (last_accessed_file == -1))
		{
			new->offset = calculated_offset - last_offset; //the offset difference, actually
			if(new->offset < 0)
				new->offset = 0 - new->offset;
			if(new->offset > MAX_PATTERN_OFFSET)
				new->offset = MAX_PATTERN_OFFSET;
		}
		else
		{
			new->offset = MAX_PATTERN_OFFSET;
		}
		last_accessed_file = i;
		last_offset = calculated_offset+(len/1024);

		//put the request in the bucket
		bucket_length++;
		agios_list_add_tail(&new->list, &bucket);

		

		//update counters
		if(type == 0)
			current_pattern->read_nb++;
		else
			current_pattern->write_nb++;

		if(bucket_length >= agios_config_pattern_compression_factor)
		{
			//summarize bucket into a single request
			new = malloc(sizeof(struct pattern_tracker_req_info_t)); //we need to allocate a new one because the one that we added to the bucket will be erased
			new->offset = summarize_bucket(); //this function already cleans the bucket
			//put the new structure in the pattern
			current_pattern->reqnb++;
			agios_list_add_tail(&new->list, &current_pattern->requests);
		}

		pthread_mutex_unlock(&pattern_tracker_lock);
	}
}

long long int summarize_bucket()
{
	struct pattern_tracker_req_info_t *req, *aux=NULL;
	long long int offset=0;
	long long int mini = LLONG_MAX;
	long long int maxi = 0;

	agios_list_for_each_entry(req, &bucket, list)
	{
		if(aux)
		{	
			agios_list_del(&aux->list);
			free(aux);
		}
		aux = req;
		if(req->offset < 0)
			req->offset = 0 - req->offset; 
		offset += req->offset;
		if(req->offset > maxi)
			maxi = req->offset;
		if(req->offset < mini)
			mini = req->offset;
	}
	if(aux)
	{	
		agios_list_del(&aux->list);
		free(aux);
	}
	if(bucket_length > 0)
		offset = offset / bucket_length;
	bucket_length = 0;
// 0 is average absolute offset distance
// 1 is minimum absolute value
// 2 is maximum absolute value
// 3 is random between minimum or maximum
	if(agios_config_pattern_compression_method == 0)
		return offset;
	else if (agios_config_pattern_compression_method == 1)
		return mini;
	else if (agios_config_pattern_compression_method == 2)
		return maxi;
	else if (agios_config_pattern_compression_method == 3)
	{
		offset = rand() % 2;
		if(offset == 0)
			return mini;
		else
			return maxi;
	}
	else //we should never arrive here (unless we have compression 0)
		return offset;
}
void free_bucket()
{
	struct pattern_tracker_req_info_t *req, *aux=NULL;

	agios_list_for_each_entry(req, &bucket, list)
	{
		if(aux)
		{	
			agios_list_del(&aux->list);
			free(aux);
		}
		aux = req;
	}
	if(aux)
	{	
		agios_list_del(&aux->list);
		free(aux);
	}
	bucket_length = 0;
}

//modify the current_pattern structure so the linked list of requests we were keeping while execution becomes an array
void translate_list_to_time_series()
{
	struct pattern_tracker_req_info_t *tmp, *aux=NULL;
	int i=0;

	current_pattern->time_series = malloc(sizeof(struct pattern_tracker_req_info_t)*(current_pattern->reqnb+1));
	if(!current_pattern->time_series)
	{
		agios_print("PANIC! Could not allocate memory for access pattern tracking\n");
		return;
	}

	agios_list_for_each_entry(tmp, &current_pattern->requests, list)
	{
		if(aux)
		{
			agios_list_del(&aux->list);
			free(aux);
		}
		current_pattern->time_series[i].offset = tmp->offset;
		i++;
		aux = tmp;
	}
	if(aux)
	{
		agios_list_del(&aux->list);
		free(aux);
	}
}

//returns the current pattern and resets it so we will start to track the next pattern
struct access_pattern_t *get_current_pattern()
{
	struct access_pattern_t *ret;
	struct pattern_tracker_req_info_t *new;

	pthread_mutex_lock(&pattern_tracker_lock);

	if(bucket_length > 0) //IMPORTANT we are adding a final incomplete bucket to the time series, we could instead want to discard it (in that case, replace all this by free_bucket() )
	{
		//summarize bucket into a single request
		new = malloc(sizeof(struct pattern_tracker_req_info_t)); //we need to allocate a new one because the one that we added to the bucket will be erased
		new->offset = summarize_bucket(); //this function already cleans the bucket
		//put the new structure in the pattern
		current_pattern->reqnb++;
		agios_list_add_tail(&new->list, &current_pattern->requests);
	}
	translate_list_to_time_series();
	ret = current_pattern;
	reset_current_pattern(0);
	pthread_mutex_unlock(&pattern_tracker_lock);
	return ret;
}

void reset_current_pattern(short int first_execution)
{
	int i;

	//initialize the current pattern (the next one to be observed)
	current_pattern = malloc(sizeof(struct access_pattern_t));
	if(!current_pattern)
	{
		agios_print("PANIC! Could not allocate memory for access pattern tracking!\n");
		return;
	}
	current_pattern->reqnb = 0;
	current_pattern->read_nb = 0;
	current_pattern->write_nb = 0;
	current_pattern->filenb = 0;
	init_agios_list_head(&current_pattern->requests);
	current_pattern->aggPtSize=NULL;
	current_pattern->time_series=NULL;
	last_offset=0;
	//clean our list of file handles
	for(i = 0; i <MAXIMUM_FILE_NUMBER; i++)
	{
		if(!first_execution) //in the first execution we simply set them all to NULL, but on future calls of this function, we may have some memory to free
		{
			if(file_ids[i])
			{
				free(file_ids[i]);
				file_ids[i] = NULL;
			}
			else
				break;
		}
		else
			file_ids[i] = NULL;
	}
	last_accessed_file = -1;
}

//init the pattern_tracker (nothing revolutionary here)
void pattern_tracker_init()
{
	struct timespec now;

	agios_is_pattern_tracking=1;
	//initialize the current access pattern
	reset_current_pattern(1);
	init_agios_list_head(&bucket);
	clock_gettime(CLOCK_MONOTONIC,&now);
	srand(now.tv_nsec);
}

//frees an access_pattern_t structure previously allocated
void free_access_pattern_t(struct access_pattern_t **ap)
{
	if(*ap)
	{
		if(!agios_list_empty(&((*ap)->requests)))
		{
			struct pattern_tracker_req_info_t *tmp, *aux=NULL;
			agios_list_for_each_entry(tmp, &((*ap)->requests), list)
			{
				if(aux)
				{
					agios_list_del(&aux->list);
					free(aux);
				}
				aux = tmp;
			}
			if(aux)
			{
				agios_list_del(&aux->list);
				free(aux);
			}
		}
		if((*ap)->time_series)
			free((*ap)->time_series);
		if((*ap)->aggPtSize)
			free((*ap)->aggPtSize);
		free(*ap);
	}

}

void pattern_tracker_exit()
{
    int i;

    if(current_pattern)
        free_access_pattern_t(&current_pattern);
    if(!agios_list_empty(&bucket))
	free_bucket();

    for(i = 0; i <MAXIMUM_FILE_NUMBER; i++)
	{
	   	if(file_ids[i])
	    	free(file_ids[i]);
        else
            break;
	}
}
