#include <pthread.h>
#include  <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "pattern_tracker.h"
#include "pattern_matching.h"


int day_carry = 0;
int last_hour = 0;
double select_algorithm_period=1;

double calculate_timestamp(int hour, int minute, double seconds)
{
	if(hour < last_hour) // a new day
		day_carry++;
	last_hour = hour;

	return (((day_carry*24) + hour)*60 + minute)*60 + seconds;
}

int main(int argc, char *argv[])
{
	FILE *fd_in;
	char line[1000];
	char *token_handle, *token_type, *token_offset, *token_len, *token, *handle, *token_timestamp;
	int type;
	long offset, len;
	int hour, minute;
	double seconds, timestamp, window_timestamp;
	int threshold, maximum_difference;

	//get arguments: input_file threshold, maximum difference
	// and open file
	if(argc < 4)
	{
		printf("Usage: ./%s\tinput_file\tpattern_matching_threshold\tmaximum_pattern_difference\n", argv[0]);
		exit(-1);
	}
	else
	{
		fd_in = fopen(argv[1], "r");
		if(!fd_in)
		{
			printf("Could not open file %s!\n", argv[1]);
			perror("fopen");
			exit(-1);
		}
		threshold = atoi(argv[2]);
		maximum_difference = atoi(argv[3]);
	}

	//start the pattern matching approach
	pattern_matching_init(1, threshold, maximum_difference);

	window_timestamp =0;
	//reads requests from the file and make them to AGIOS
	while(fgets(line, 1000, fd_in))
	{
		//separate request information to give to AGIOS
		//1. by ',' in big parts
		token_timestamp = strtok(line, ",");
		token_handle = strtok(NULL, ",");
		token_type = strtok(NULL, ",");
		token_type = strtok(NULL, ",");
		token_offset = strtok(NULL, ",");
		token_len = strtok(NULL, ",");
		//2 let's get the handle
		token = strtok(token_handle, ": ");
		handle = strtok(NULL, ": ");
		//3 type
		token = strtok(token_type, ": ");
		type = atoi(strtok(NULL, ": "));
		//4 offset
		token = strtok(token_offset, ": ");
		offset = atol(strtok(NULL, ": "));
		//5 len
		token = strtok(token_len, ": ");
		len = atol(strtok(NULL, ": "));
		//6 timestamp
		token = strtok(token_timestamp, "]");
		token_timestamp = strtok(token, "D ");
		token_timestamp = strtok(NULL, "D ");
		hour = atoi(strtok(token_timestamp, ":"));
		minute = atoi(strtok(NULL, ":"));
		seconds = atof(strtok(NULL, ":"));
		timestamp = calculate_timestamp(hour, minute, seconds);
		if(window_timestamp == 0)
			window_timestamp = timestamp;
		if(timestamp - window_timestamp >= select_algorithm_period)
		{
			pattern_matching_select_next_algorithm();
			window_timestamp = timestamp;
		}
		add_request_to_pattern((long long int)(timestamp*1000000.0), offset, len, type, handle);
	}
	pattern_matching_select_next_algorithm();
	fclose(fd_in);
	//stop agios and finish
	pattern_matching_exit();
	return 0;
}

