import subprocess

ionodes=[1, 2, 4] 
ops = ["read", "write"]
prefix="/home/fran/ioforw_traces/data" #where the traces are


#first we need to know the translation from test label to its access pattern
pattern={}
pattern_ion={}
arq = open("test_info.csv", "r")
for line in arq:
	if "label" in line:
		continue
	parsed = line.split('\n')[0].split(';')
	if parsed[1] == "8":  #I removed these results because we had too much data 
		continue
	if parsed[0] in pattern:
		print("repeated patterns???")
		exit()
	pattern[parsed[0]]=parsed[2]+ ";"+parsed[3]+";"+parsed[4]+";"+parsed[5]+";"+parsed[7]
	pattern_ion[parsed[0]] = int(parsed[1])
arq.close()

#create the output file
csv = open("pattern_count.csv", "w")
csv.write("operation;ionodes;server;repetition;proc;files;contig;reqsize;pat_number\n")
buf = ""   #to avoid small writes
buflimit = 1048576

for op in ops:
	for ion in ionodes:
		for i in range(1, ion+1):
			patnb={}
			filename = "all_comparisons."+op+"."+str(ion)+"ionodes.node"+str(i)+".txt"
			print(filename)
			arq = subprocess.getoutput("cat "+filename+" | grep "+prefix).split('\n')
			for line in arq:
				parsed = line.split()[0].split('/')
				parsed = parsed[len(parsed)-3].split('-')
				label = parsed[len(parsed)-1]
				if pattern_ion[label] != ion:
					print("we are looking at results with "+str(ion)+" io nodes, but label "+label+" was obtained with "+str(pattern_ion[label])+" io nodes...")
					exit()
				coisa = int(label) #just to see we got the right thing
				if not (label in  pattern):
					print("we don't know this pattern????")
					print(line)
					exit()
				if label in patnb:
					patnb[label]+= 1
				else:
					#if it is the first pattern of a file, we already finished the previous file
					if len(patnb) > 1:
						print("how come we have more than one label?")
						print(patnb)
						exit()
					for other in patnb:
						buf += op+";"+str(ion)+";"+str(i)+";"+pattern[other]+";"+str(patnb[other])+"\n"
						if len(buf) > buflimit:
							csv.write(buf)
							buf = ""
					patnb={}
					patnb[label]=1
			for other in patnb:
				buf += op+";"+str(ion)+";"+str(i)+";"+pattern[other]+";"+str(patnb[other])+"\n"
				if len(buf) > buflimit:
					csv.write(buf)
					buf = ""


				
csv.write(buf)
buf=""
csv.close()
					


