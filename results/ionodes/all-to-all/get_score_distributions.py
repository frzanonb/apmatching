class OutputFile:
	def __init__(self, filename):
		#to avoid small writes to disk
		self.buf = ""
		self.buflimit = 1048576 
		self.arq = open("distance_distr_"+filename+".csv", "w") 
	def write(self, value):
		self.buf += value+"\n"
		if len(self.buf) > self.buflimit:
			self.arq.write(self.buf)
			self.buf = ""
	def close(self):
		self.arq.write(self.buf)
		self.arq.close()
	


ionodes=[1, 2, 4] 
ops = ["read", "write"]
fs = ["nto1", "nton"]
compression = 0


#first we need to know the translation from test label to its access pattern
pattern={}
arq = open("test_info.csv", "r")
for line in arq:
	if "label" in line:
		continue
	parsed = line.split('\n')[0].split(';')
	if parsed[1] != "1":  #I removed these results because we had too much data 
		continue
	if parsed[0] in pattern:
		print("repeated patterns???")
		exit()
	pattern[parsed[0]]= {"ion": int(parsed[1]), "files": parsed[4], "contig": int(parsed[5]), "reqsize": parsed[7]}  #repetition is not being considered
arq.close()

#we will have output files separated by if patterns should match or not, and for the ones that should match separated by test configuration (operation, number of I/O nodes, nto1 or nto1, contig or not
dontmatch={}
dontmatch["read"] = OutputFile("dontmatch_read")
dontmatch["write"] = OutputFile("dontmatch_write")
match = {}
for op in ops:
	match[op] = {}
	for ion in ionodes:
		match[op][ion] = {}
		for files in fs:
			match[op][ion][files]={}
			for contig in [0,1]:
				if contig:
					contig_str = "contig"
				else:
					if files == "nton":
						continue
					contig_str = "noncontig"
				match[op][ion][files][contig]= OutputFile("match_"+op+"_"+str(ion)+"ionodes_"+files+"_"+contig_str)
			
		


#now we can read the measured distances. We have to keep them (instead of directly writing to output) because we learn where the patterns came while reading (we only have the full information in the last pattern). But to avoid that large memory footprint, we write results as soon as we can
for op in ops:
	for ion in ionodes:
		for i in range(1, ion+1):
		#for i in [4]:
			distances={}
			#read the file
			filename = "all_comparisons."+op+"."+str(ion)+"ionodes.comp"+str(compression)+".node"+str(i)+".txt"
			print(filename)
			arq = open(filename, "r")
			patnb = int(arq.readline().split()[0])
			for p1 in range(0, patnb):
				#the first line says the file from where the pattern was read, and the pattern number
				line = arq.readline()
				parsed = line.split()[0].split('/')
				parsed = parsed[len(parsed)-3].split('-')
				label = parsed[len(parsed)-1]
				if pattern[label]["ion"] != ion:
					print("we are looking at results with "+str(ion)+" io nodes, but label "+label+" was obtained with "+str(pattern[label]["ion"])+" io nodes...")
					exit()
				coisa = int(label) #just to see we got the right thing
				if not (label in  pattern):
					print("we don't know this pattern????")
					print(line)
					exit()
				p0 = int(line.split('\n')[0].split()[1])
				if p0 != p1:
					print("the files are not organized the way we were expecting...")
					print(line)
					exit()
				plabel = label+"_"+str(p1)  #an unique identifier to our data structure
				if plabel in distances:
					print("reading twice for the same pattern???")
					print(line)
					exit()
				#now we have new information, where the pattern p1 came from, so whatever we had waiting for this can now be written to the file
				for first in distances:
					if p1 in distances[first]:
						other = first.split('_')[1]
						otherlabel = first.split('_')[0]
						#we see if the patterns should match or not to see to what file we should write
						if (pattern[otherlabel]["files"] != pattern[label]["files"]) or (pattern[otherlabel]["contig"] != pattern[label]["contig"]) or (pattern[otherlabel]["reqsize"] != pattern[label]["reqsize"]):
							dontmatch[op].write(distances[first][p1])
						else:
							match[op][ion][pattern[label]["files"]][pattern[label]["contig"]].write(distances[first][p1])
						del distances[first][p1]
				#now read the distances
				if p1 == (patnb-1):
					break #we are done
				distances[plabel] = {}
				for p2 in range(p1+1, patnb):
					if p2 in distances[plabel]:
						print("reading twice for the same pair???")
						print(line)
						exit()
					line = arq.readline()
					parsed = line.split('\n')[0].split()
					if int(parsed[1]) != p2:
						print("the files are not organized the way we were expecting...")
						print(line)
						exit()
					#keep the distance in our data structure for when we have the whole information (notably we don't know the label of p2)
					distances[plabel][p2] = str(int(parsed[2]))
			for first in distances:
				for p2 in distances[first]:
					print("panic! we did not solve the pair "+first+" "+str(p2))
					exit()
					

dontmatch["read"].close()	
dontmatch["write"].close()
for op in ops:
	for ion in ionodes:
		for files in fs:
			for contig in [0,1]:
				if (contig == 0) and (files == "nton"):
					continue
				match[op][ion][files][contig].close()
			


