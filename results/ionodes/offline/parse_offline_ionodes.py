import commands
import sys
from numpy import max,mean

# dir is something like multiplepatterns_read_maxdiff70_threshold95_1ionodes_node0
# return a dict with parameters
def parse_test_name(dir):
	ret = {}
	lista = dir.split('/')[0].split('_')
	ret["operation"] = lista[1]
	ret["maxdiff"] = int(lista[2].split('f')[2])
	ret["threshold"] = int(lista[3].split('d')[1])
	ret["ionodes"] = int(lista[4].split('i')[0])
	ret["node"] = int(lista[5].split('e')[1])
	ret["repetition"] = int(lista[6].split('t')[1])
	#sanity check
	if ((ret["operation"] != "write") and (ret["operation"] != "read")):
		print "PANIC! I don't understand this test name...",
		print dir
		exit()
	return ret
#receive the test information obtained with the parse_test_name function and returns a string to be written to the csv 
def get_test_info_str(test):
	return str(test["ionodes"])+";"+test["operation"]+";"+str(test["maxdiff"])+";"+str(test["threshold"])+";"+str(test["node"])+";1;"+str(compression)+";"+str(test["repetition"])+";"

#receive a pattern (like the ones generated in the make_tests_list function) and gives back the string to be written to the csv about it (files;contig;reqsize)
def get_pattern_str(id):
	parsed = id.split('_')
	return parsed[0]+";"+parsed[1]+";"+parsed[2]

#returns the sum of a list of numbers:
def sum_of_list(lista):
	soma = 0
	for elem in lista:
		soma += elem
	return soma

#line is something like AGIOS: PATTERN_MATCHING_select_next_algorithm(): We did not find a match, stored a new pattern with id 0
# return the id
def get_new_id(line):
	lista = line.split('\n')[0].split(' ')
	return int(lista[len(lista)-1])

#line is something like AGIOS: write_pattern_matching_file(): We'll write the pattern_matching file. We have 3 patterns (from 3 calls in this execution, average size 682), max DTW score is 897397. We've called FastDTW 3 times to patterns of average size 927
# return a dict with information
def get_final_information(line):
	ret = {}
	lista = line.split('\n')[0].split(' ')
	ret["known_patterns"] = int(lista[9])
	ret["pm_call_count"] = int(lista[12])
	ret["max_dtw_score"] = int(lista[24].split('.')[0])
	ret["fastdtw_call_count"] = int(lista[28])
	ret["avg_call_size"] = int(lista[len(lista)-1])
	return ret

# line is something like "AGIOS: PATTERN_MATCHING_select_next_algorithm(): Found a match with id 9
# return id
def get_matched_id(line):
	lista = line.split('\n')[0].split(' ')
	ret = int(lista[len(lista)-1])
	return ret

#returns a list of access patterns included in a test and a dict of label to access pattern
def make_tests_list(ionodes_nb):
	patterns = []
	labels = {}
	#read the test information to know to what execution each test belongs
	arq = open("test_info.csv", "r")
	for line in arq:
		if "label" in line:
			continue
		parsed = line.split('\n')[0].split(';')
		ion = int(parsed[1])
		if not (ion in ionodes_nb):
			continue
		label = parsed[0]
		assert not (label in labels)
		ion = int(parsed[1])
		assert ion in ionodes_nb
		files = parsed[4]
		contig = parsed[5]
		reqsize = parsed[7]
		this_pattern = files + "_" + contig + "_" + reqsize #we dont include the number of i/o nodes or from what i/o node (or the operation) in this pattern representation because we never mix these results in the same tests, so no need to deduplicate here
		labels[label] = this_pattern
		if not (this_pattern in patterns):
			patterns.append(this_pattern)
	return (labels, patterns)

#line is something like "/home/fran/ioforw_traces/data/logs-experiment.pattern.matching.4gb.to-209/agios-log-grisou-2.nancy.grid5000.fr/agios_tracefile.1.out.read"
#return the label of the test
def get_label(line):
	lista = line.split('/')
	i = 0
	while not "logs-experiment" in lista[i]:
		i+=1
	assert i < (len(lista)-1)
	ret = lista[i].split('-')[2]
	coisa = int(ret) #just to be sure we got the right thing
	return ret

############################################################################################
#take the list of directories
root = sys.argv[1]
tests = commands.getoutput("ls "+root+"/ |grep multiplepatterns_").split('\n')
if ("No such" in tests[0]) or ("encontrado" in tests[0]):
	print "PANIC! No files?"
	exit()
#remove scripts from the list of directories
rem = []
for elem in tests:
	if (".py" in elem) or (".sh" in elem) or (".csv" in elem):
		rem.append(elem)
for elem in rem:
	tests.remove(elem)

#we'll write results to a csv file
csv = open("ionodes_multiapp_evaluation.csv", "w")
csv.write("ionodes_nb;operation;maxdiff;threshold;ionode;window_size;compression;repetition;true_negative;false_negative;true_positive;false_positive;known_pat;fastdtw_calls;fastdtw_size\n")
#and we'll also create a file with the detected access patterns (so we can translate into performance improvements)
detections = open("ionodes_multiapp_detectedap.csv", "w")
detections.write("ionodes_nb;operation;maxdiff;threshold;ionode;window_size;compression;repetition;files;contig;reqsize;files_det;contig_det;reqsize_det\n")

#we need a list of the access patterns we know, we'll also keep a dict to know, from a label, what access pattern it comes from
ionodes_nb = [1,2,4]
labels, existing_tests = make_tests_list(ionodes_nb)

prefix="/home/fran" #the prefix used in the multiplepatterns_ script (without the ending / )
compression = 100

#go through them
debug = 0
for dir in tests:
	print dir
	#parse the directory name for test parameters
	test = {}
	test = parse_test_name(dir)
	#read  the test trace
	arq = open(root+"/"+dir+"/test_trace.txt", "r")
	execution = 0
	linenb = 0
	pattern_list = {} #this stores the known ids of expected patterns, we'll separate inside by test
	for p in existing_tests:
		pattern_list[p] = []
	idtopattern = {} #this stores the mapping from ids (from the pattern matching mechanism) to ids from this script, which are string representations of the access patterns (files;contig;reqsize)
	cumulative_false_neg = []
	cumulative_true_neg = []
	cumulative_false_pos = []
	cumulative_true_pos = []
	cumulative_known_pat = []
	cumulative_fastdtw_calls = []
	cumulative_fastdtw_size = []
	false_negative = 0
	true_negative=0
	false_positive = 0
	true_positive = 0
	id = ""
	detections_str = ""
	for linha in arq:
		linenb+=1
		if "logs-experiment.pattern.matching.4gb" in linha: #line with the name of the trace, we'll get the identifier from it
			id = labels[get_label(linha)]
			if debug == 1:
				print "in the new execution trace is",
				print id
		if "dtw_test_ionodes "+prefix+"/trace.txt" in linha: #new execution
			execution += 1	
			#reset counters	
			pattern=-1
			true_positive = 0
			false_positive = 0
			true_negative = 0
			false_negative = 0
			ignored_count = 0
			predictions=0
			right_predictions = 0
			wrong_predictions = 0
			compatible_predictions = 0
			if debug == 1:
				print "execution",
				print execution
		elif "Could not open pattern matching file" in linha: #this is not supposed to happen after the first execution
			if execution > 1:
				print dir
				print "line ",
				print linenb
				print "Something went wrong."
				arq.close()
				csv.close()
				exit()
		elif "patterns from the pattern matching file, the maximum DTW score so far" in linha: #this is not supposed to happen in the first execution
			if execution == 1:
				print dir
				print "line ",
				print linenb
				print "Something went wrong."
				arq.close()
				csv.close()
				exit()
		elif "PATTERN_MATCHING_select_next_algorithm(): Adding a pattern" in linha: #we have a pattern, will try to match it
			pattern+=1
#BCK			if pattern >= len(pattern_list[id]):
#BCK				pattern_list[id].append([])
			if debug == 1:
				print "new pattern, current is number",
				print pattern,
#BCK				print pattern_list[id]
		elif "AGIOS: PATTERN_MATCHING_select_next_algorithm(): We'll ignore" in linha:
			#this pattern was not a pattern
			ignored_count+=1
			#undo the new pattern part
#BCK			if len(pattern_list[id][pattern]) == 0: 
#BCK				pattern_list[id].pop()
			pattern-=1
			if debug == 1:
				print "ignoring a pattern, rollback to",
				print pattern,
#BCK				print pattern_list[id]
		elif "We did not find a match, stored a new pattern" in linha: #new id for the same pattern
			new_id = get_new_id(linha)
#BCK			pattern_list[id][pattern].append(new_id) #this is now a known id for this pattern
			pattern_list[id].append(new_id) #this is now a known id for this pattern
			#if we do not have a match, it could be a false or a true negative
#BCK			if len(pattern_list[id][pattern]) == 1: #this was the first time this pattern was seen, it was not supposed to match
			if len(pattern_list[id]) == 1: #this was the first time this pattern was seen, it was not supposed to match
				true_negative+=1
			else: #we have failed to match 
				false_negative+=1
			detections_str += get_test_info_str(test,compression) + get_pattern_str(id) + ";-;-;-\n"
			assert not (new_id in idtopattern)
			idtopattern[new_id] = id
			if debug == 1:
				print "no match, negatives - true",
				print true_negative,
				print "false",
				print false_negative	
		elif "AGIOS: PATTERN_MATCHING_select_next_algorithm(): Found a match with id" in linha: #we have a match
			new_id = get_matched_id(linha)
			#if we have a match, it could be a true or a false positive
#BCK			if len(pattern_list[id][pattern]) == 0: #in the first occurrence of a pattern we assume match is correct
#BCK				true_positive += 1
#BCK				pattern_list[id][pattern].append(new_id)
#BCK			elif new_id in pattern_list[id][pattern]: #match with a known id for the same pattern
			if new_id in pattern_list[id]: #match with a known id for the same pattern
				true_positive+=1
			else: #match with something else, should not be a match
				false_positive+=1
			assert new_id in idtopattern
			detections_str += get_test_info_str(test,compression) + get_pattern_str(id) + ";" + get_pattern_str(idtopattern[new_id])+"\n"
			if debug == 1:
				print "match with",
				print new_id,
				print "positives - true",
				print true_positive,
				print "false",
				print false_positive
#TODO
		elif "AGIOS: write_pattern_matching_file(): We'll write" in linha: #finishing this execution, get information from it
			final_info = get_final_information(linha)
			#we acumulate the metrics observed for this execution
			cumulative_false_neg.append(false_negative)
			cumulative_true_neg.append(true_negative)
			cumulative_false_pos.append(false_positive)
			cumulative_true_pos.append(true_positive)
			cumulative_known_pat.append(final_info["known_patterns"])
			cumulative_fastdtw_calls.append(final_info["fastdtw_call_count"])
			cumulative_fastdtw_size.append(final_info["avg_call_size"])
			if debug == 1:
				print "ending this execution\n---------------------------------------------------------------------------------"	
		else:
			continue
	arq.close()
	#now we can write the cumulative metrics from this test to the csv file
	csv.write(get_test_info_str(test,compression)) #write "ionodes_nb;operation;maxdiff;threshold;ionode;window_size;compression;"	
	#write "true_negative;false_negative;true_positive;false_positive;fastdtw_calls;fastdtw_size\n"
	csv.write(str(sum_of_list(cumulative_true_neg))+";"+str(sum_of_list(cumulative_false_neg))+";"+str(sum_of_list(cumulative_true_pos))+";"+str(sum_of_list(cumulative_false_pos))+";"+str(max(cumulative_known_pat))+";"+str(sum_of_list(cumulative_fastdtw_calls))+";"+str(mean(cumulative_fastdtw_size))+"\n")
	detections.write(detections_str)
	
csv.close()
detections.close()
