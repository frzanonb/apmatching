import os
import time
import errno
import argparse

from TWINSCouncilDatabaseAP import TWINSCouncilDatabaseAP

# Configure the parser
parser = argparse.ArgumentParser()

parser.add_argument('--debug',
						action = 'store_true',
						dest = 'debug',
						help = 'Run TWINS prediction service in debug mode')

parser.add_argument('--file',
						action = 'store',
						dest = 'file',
						required = True,
						help = 'Define the file')


# Parse the arguments
args = parser.parse_args()

# Create the service
server = TWINSCouncilDatabaseAP(args.debug)

server.run(args.file)