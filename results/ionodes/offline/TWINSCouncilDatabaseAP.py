#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path
import datetime
import random
import sys
import csv
import time
import string
import shutil
import socket
import select
import errno
import traceback

import glob
import logging
import logging.handlers

import numpy as np
import pandas as pd

from collections import defaultdict


class TWINSCouncilDatabaseAP:

	LOG_FILENAME = 'twins-council-ap.log'

	DATABASE = {}
	DATABASE_BANDWIDTH = {}

	STATIC_WINDOW = 125
	BASELINE_WINDOW = 1000

	WINDOW_SIZES = [125, 250, 500, 1000, 2000, 4000, 8000]

	# Construtor
	def __init__(self, debug):

		self.configureLog(debug)
		self.loadDatabase()


	# Configure the log system
	def configureLog(self, debug):
		# Creates and configure the log file
		self.logger = logging.getLogger('TWINSCouncilDatabaseAP')

		if debug:
			self.logger.setLevel(logging.DEBUG)
		else:
			self.logger.setLevel(logging.INFO)

		# Defines the format of the logger
		formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

		# Configure the log rotation
		handler = logging.handlers.RotatingFileHandler(self.LOG_FILENAME, maxBytes = 268435456, backupCount = 50, encoding = 'utf8')

		handler.setFormatter(formatter)

		self.logger.addHandler(handler)

		self.logger.info('Starting TWINS Council')


	def loadDatabase(self):
		# Read the original CSV file
		data = csv.reader(open('input/database-bandwidth.csv', 'r'))

		self.logger.info('Parsing initial access pattern database')

		for i, line in enumerate(data):
			# Skip the header
			if i == 0:
				continue

			signature = '|'.join(line[1:-1])
			twins = line[-2]
			bandwidth = line[-1]

			self.DATABASE[signature] = twins
			self.DATABASE_BANDWIDTH[signature] = bandwidth

		self.logger.info('Database has {} access patterns known'.format(len(self.DATABASE)))


	def run(self, path):
		# Read all the detecte access patterns
		# path = '/home/jbez/Downloads/ionodes_detectedap_plot.csv'

		# Read all the observations from the CSV
		df = pd.read_csv(path, index_col = 0)

		# Compute statistics
		statistics = {}

		# Iterate over each one of them and predict the best window size for each one
		for index, row in df.iterrows():
			# Convert the message we received into an array of metrics to feed to the dataframe
			observation = np.array([row])

			# We are receiving the following metrics from the file:
			# 		    0           1         2         3        4            5           6          7      8        9       10       11            12           13 
			#  ionodes_nb	operation	maxdiff	threshold	ionode	window_size	compression	repetition 	files	contig	reqsize	files_det	contig_det	reqsize_det
			#	        1      "read"        40        99        0            1         100          1 "nto1"        1   262144    "nto1"          "1"     "262144"
			self.logger.debug('Metrics: {}'.format(observation))

			# Transform the CSV data to our predefined access pattern labels

			# Original value
			if (str(observation[0][8]) == 'nton' and str(observation[0][9]) == '1'):
				value = 'FP'

			elif (str(observation[0][8]) == 'nto1' and str(observation[0][9]) == '0'):
				value = 'SS'

			elif (str(observation[0][8]) == 'nto1' and str(observation[0][9]) == '1'):
				value = 'SC'

			else:
				# In case we do not detect the pattern, we will use the default window size
				value = '-'

			# Determined value
			if (str(observation[0][11]) == 'nton' and str(observation[0][12]) == '1'):
				value_det = 'FP'

			elif (str(observation[0][11]) == 'nto1' and str(observation[0][12]) == '0'):
				value_det = 'SS'

			elif (str(observation[0][11]) == 'nto1' and str(observation[0][12]) == '1'):
				value_det = 'SC'

			else:
				# In case we do not detect the pattern, we will use the default window size
				value_det = '-'

			self.logger.debug('Access pattern: {} - {} (det)'.format(value, value_det))

			# OPTIMAL
			window_optimal = 0
			bandwidth_optimal = 0

			for size in self.WINDOW_SIZES:
				# Get the optimal window size
				configuration = [
					4,											# pvfs_servers
					observation[0][0], 							# iofsl_servers
					32, 										# clients
					128, 										# processes
					observation[0][10], 						# size
					1 if observation[0][1] == 'read' else 0,	# read
					value,										# access_pattern
					size 										# window_size
				]

				# Generate the signature for the access pattern
				signature = '|'.join(map(str, configuration))

				b = self.DATABASE_BANDWIDTH[signature]

				if float(b) > float(bandwidth_optimal):
					bandwidth_optimal = b
					window_optimal = size


			# DETECTED (PATTERN MATCHING)
			# Get the best window size for the pattern (files_det, contig_det, reqsize_det), let's assume it is N
			# Then we should report the bandwidth with N for the pattern (files, contig, reqsize) that is what is happening at the moment
			window_det = 0
			bandwidth_det = 0

			for size in self.WINDOW_SIZES:
				# Get the optimal window size
				configuration = [
					4,											# pvfs_servers
					observation[0][0], 							# iofsl_servers
					32, 										# clients
					128, 										# processes
					observation[0][13], 						# size (det)
					1 if observation[0][1] == 'read' else 0,	# read
					value_det,									# access_pattern (det)
					size 										# window_size
				]

				# Generate the signature for the access pattern
				signature = '|'.join(map(str, configuration))
				
				if signature in self.DATABASE_BANDWIDTH:
					b = self.DATABASE_BANDWIDTH[signature]

					if float(b) > float(bandwidth_det):
						bandwidth_det = b
						window_det = size


			if (window_det == 0):
				window_det = self.BASELINE_WINDOW

			# Now we need to get the bandwith based on the pattern we are running, and not the detected
			configuration_detected = [
				4,												# pvfs_servers
				observation[0][0], 								# iofsl_servers
				32, 											# clients
				128, 											# processes
				observation[0][10], 							# size
				1 if observation[0][1] == 'read' else 0,		# read
				value,											# access_pattern (det)
				window_det										# window_size
			]

			# Generate the signature for the access pattern
			signature_detected = '|'.join(map(str, configuration_detected))

			bandwidth_det = self.DATABASE_BANDWIDTH[signature_detected]

			# STATIC
			configuration_static = [
				4,											# pvfs_servers
				observation[0][0], 							# iofsl_servers
				32, 										# clients
				128, 										# processes
				observation[0][10], 						# size
				1 if observation[0][1] == 'read' else 0,	# read
				value,										# access_pattern
				self.STATIC_WINDOW
			]

			# Generate the signature for the access pattern
			signature_static = '|'.join(map(str, configuration_static))
			
			window_static = self.STATIC_WINDOW
			bandwidth_static = self.DATABASE_BANDWIDTH[signature_static]

			# BASELINE
			configuration_baseline = [
				4,											# pvfs_servers
				observation[0][0], 							# iofsl_servers
				32, 										# clients
				128, 										# processes
				observation[0][10], 						# size
				1 if observation[0][1] == 'read' else 0,	# read
				value,										# access_pattern
				self.BASELINE_WINDOW
			]

			# Generate the signature for the access pattern
			signature_baseline = '|'.join(map(str, configuration_baseline))
			
			window_baseline = self.BASELINE_WINDOW
			bandwidth_baseline = self.DATABASE_BANDWIDTH[signature_baseline]

			# Append the data in the CSV
			df.loc[index, 'window_baseline'] = window_baseline
			df.loc[index, 'window_optimal'] = window_optimal
			df.loc[index, 'window_static'] = window_static
			df.loc[index, 'window_det'] = window_det

			df.loc[index, 'bandwidth_baseline'] = bandwidth_baseline
			df.loc[index, 'bandwidth_optimal'] = bandwidth_optimal
			df.loc[index, 'bandwidth_static'] = bandwidth_static
			df.loc[index, 'bandwidth_det'] = bandwidth_det

		output = '{}-twins.csv'.format(path.replace('.csv', ''))

		df.to_csv(output, sep=';', header = False)

