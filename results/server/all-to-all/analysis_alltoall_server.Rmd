---
title: "analysis - all to all - server traces"
author: "Francieli Zanon Boito"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(plyr)
library(ggplot2)
```

#characterization of the patterns from this dataset

```{r}
pat_count <- read.csv("pattern_count.csv", sep=";", header=T)
pat_len <- read.csv("pattern_length.csv", sep=";", header=T)
```


```{r}
pat_count <- subset(pat_count, compression == 10 | compression == 100) #so we dont count read pattern twice
sum(pat_count$pat_number)

sum(subset(pat_count, operation == "read")$pat_number)
sum(subset(pat_count, operation == "write")$pat_number)

sum(subset(pat_count, files1=="nto1")$pat_number)
sum(subset(pat_count, files1=="nton")$pat_number)

sum(subset(pat_count, contig1 == "contig")$pat_number)
sum(subset(pat_count, contig1 == "noncontig")$pat_number)
```
9216 patterns, 1779 from read experiments and 7437 from write experiments. 5063 from shared file and 4153 from file-per-process, 5154 from contiguous experiments and 4062 with 1D-strided

```{r}
pat_len$avg_pat_length = pat_len$pat_len 

summary(pat_len$avg_pat_length)

summary(subset(pat_len, operation == "read")$avg_pat_length)
summary(subset(pat_len, operation == "write")$avg_pat_length)

summary(subset(pat_len, files1=="nto1")$avg_pat_length)
summary(subset(pat_len, files1=="nton")$avg_pat_length)

summary(subset(pat_len, contig1 == "contig")$avg_pat_length)
summary(subset(pat_len, contig1 == "noncontig")$avg_pat_length)

```


Patterns of ~59 to ~14389 requests (median of 605). Read patterns are longer (median of 734 for read and 551 for write, maximum of 14389 for reads and 3008 for writes)
shared-file patterns are longer (median of 846 and maximum of 14389 vs. median of 242 and maximum of 957)
1D strided patterns are longer (median of 923 and maximum of 14389 vs median of 554 and maximum of 1098)

# All-to-all comparisons

```{r}
alltoall <- read.csv("all_compare.csv", header=T, sep=";")
alltoall$distance <- alltoall$score  #I gave the column the wrong name while parsing the results
alltoall$score <- NULL
```

## no compression

```{r}
alltoall0 <- subset(alltoall, compression == 0 & infinite == "10GB")
```

general distribution of distances

```{r}
summary(alltoall0$distance)

summary(subset(alltoall0, operation1 == "read")$distance)
summary(subset(alltoall0, operation1 == "write")$distance)
```

distances between read patterns are larger than between write patterns

is there a difference between distances that should match and the ones that should not?

```{r}
summary(subset(alltoall0, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2)$distance)
summary(subset(alltoall0, spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2)$distance)
```

the distances between good matches are smaller than the bad matches, it is not clear if maybe we can separate the two groups

what happens when we calculate score?
```{r}
maxi <- max(alltoall0$distance)
max_read <- max(subset(alltoall0, operation1 == "read")$distance)
max_write <- max(subset(alltoall0, operation1 == "write")$distance)
alltoall0$unique_score <- 1.0 - alltoall0$distance/maxi
alltoall0$score <- ifelse(alltoall0$operation1 == "read", 1.0 - alltoall0$distance/max_read, 1.0 - alltoall0$distance/max_write)
```

```{r}
summary(subset(alltoall0, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2)$unique_score)
summary(subset(alltoall0, spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2)$unique_score)

summary(subset(alltoall0, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2)$score)
summary(subset(alltoall0, spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2)$score)
```

there is a threshold that allows to take over 75% of the right matches, but we take like one third of the wrong matches

is it different for reads and writes?

```{r}
#write 
summary(subset(alltoall0, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write")$unique_score)
summary(subset(alltoall0, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "write")$unique_score)

summary(subset(alltoall0, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write")$score)
summary(subset(alltoall0, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "write")$score)

#read 
summary(subset(alltoall0, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read")$unique_score)
summary(subset(alltoall0, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")$unique_score)

summary(subset(alltoall0, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read")$score)
summary(subset(alltoall0, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")$score)
```

it is worse for reads

## compression factor of 10

```{r}
alltoall10 <- subset(alltoall, compression == 10 & infinite == "10GB")
```

general distribution of distances

```{r}
summary(alltoall10$distance)

summary(subset(alltoall10, operation1 == "read")$distance)
summary(subset(alltoall10, operation1 == "write")$distance)
```

is there a difference between distances that should match and the ones that should not?

```{r}
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2)$distance)
summary(subset(alltoall10, spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2)$distance)

```

yes, over 75% of the cases that should match have distance smaller than the median of distances that do not match

what happens when we calculate score?
```{r}
maxi <- max(alltoall10$distance)
max_read <- max(subset(alltoall10, operation1 == "read")$distance)
max_write <- max(subset(alltoall10, operation1 == "write")$distance)
alltoall10$unique_score <- 1.0 - alltoall10$distance/maxi
alltoall10$score <- ifelse(alltoall10$operation1 == "read", 1.0 - alltoall10$distance/max_read, 1.0 - alltoall10$distance/max_write)
```

```{r}
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2)$unique_score)
summary(subset(alltoall10, spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2)$unique_score)

summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2)$score)
summary(subset(alltoall10, spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2)$score)
```

The unique scores with a threshold of 0.9935 allows to match 75% of the right cases while matching over 25% of the wrong matches
 
is it different for reads and writes?

```{r}
#write 
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write")$unique_score)
summary(subset(alltoall10, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "write")$unique_score)

summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write")$score)
summary(subset(alltoall10, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "write")$score)

#read 
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read")$unique_score)
summary(subset(alltoall10, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")$unique_score)

summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read")$score)
summary(subset(alltoall10, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")$score)
```

What are the expected results?

```{r}
#write 
match <- subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write")
dontmatch <- subset(alltoall10, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "write")
#threshold of 0.98 
len_match <- length(match$distance)
len_dontmatch <- length(dontmatch$distance)
threshold = 0.98
length(subset(match, unique_score >= threshold)$distance)/len_match
length(subset(dontmatch, unique_score >= threshold)$distance)/len_dontmatch
length(subset(match, score >= threshold)$distance)/len_match
length(subset(dontmatch, score >= threshold)$distance)/len_dontmatch
#threshold of 0.99 
threshold = 0.99
length(subset(match, unique_score >= threshold)$distance)/len_match
length(subset(dontmatch, unique_score >= threshold)$distance)/len_dontmatch
length(subset(match, score >= threshold)$distance)/len_match
length(subset(dontmatch, score >= threshold)$distance)/len_dontmatch

#read 
match <- subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read")
dontmatch <- subset(alltoall10, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")
#threshold of 0.98 
len_match <- length(match$distance)
len_dontmatch <- length(dontmatch$distance)
threshold = 0.98
length(subset(match, unique_score >= threshold)$distance)/len_match
length(subset(dontmatch, unique_score >= threshold)$distance)/len_dontmatch
length(subset(match, score >= threshold)$distance)/len_match
length(subset(dontmatch, score >= threshold)$distance)/len_dontmatch
#threshold of 0.99 
threshold = 0.99
length(subset(match, unique_score >= threshold)$distance)/len_match
length(subset(dontmatch, unique_score >= threshold)$distance)/len_dontmatch
length(subset(match, score >= threshold)$distance)/len_match
length(subset(dontmatch, score >= threshold)$distance)/len_dontmatch
```

for reads, to capture 75% of the right matches we need to incorrectly match over 50% of the wrong matches
 
### Do these results depend on the access pattern?
 
```{r}
#dont match write 
summary(subset(alltoall10, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "write")$unique_score)
summary(subset(alltoall10, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "write")$score)

#match write contig 
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write" & spatiality1 == "contig")$unique_score)
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write" & spatiality1 == "contig")$score)

#match write noncontig 
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write" & spatiality1 == "noncontig")$unique_score)
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write" & spatiality1 == "noncontig")$score)

#match write nto1 
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write" & filesnb1 == "nto1")$unique_score)
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write" & filesnb1 == "nto1")$score)

#match write nton 
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write" & filesnb1 == "nton")$unique_score)
summary(subset(alltoall10, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write" & filesnb1 == "nton")$score)
```

no, they all seem similar

## compression factor of 100

```{r}
alltoall100_512mb <- subset(alltoall, compression == 100 & infinite == "512MB")
alltoall100_10gb <- subset(alltoall, compression == 100 & infinite == "10GB")
alltoall100_100gb <- subset(alltoall, compression == 100 & infinite == "100GB")
```

general distribution of distances

```{r}
summary(alltoall100_10gb$distance)

summary(subset(alltoall100_10gb, operation1 == "read")$distance)
summary(subset(alltoall100_10gb, operation1 == "write")$distance)
```

is there a difference between distances that should match and the ones that should not?

```{r}
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2)$distance)
summary(subset(alltoall100_10gb, spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2)$distance)
```

yes, we can see it (although there is still an overlapping of ~25%)

what happens when we calculate score?
```{r}
calculate_score <- function(orig_df)
{
  df <- orig_df
  maxi <- max(df$distance)
  max_read <- max(subset(df, operation1 == "read")$distance)
  max_write <- max(subset(df, operation1 == "write")$distance)
  df$unique_score <- 1.0 - df$distance/maxi
  df$score <- ifelse(df$operation1 == "read", 1.0 - df$distance/max_read, 1.0 - df$distance/max_write)
  return(df)
}
alltoall100_512mb <- calculate_score(alltoall100_512mb)
alltoall100_10gb <- calculate_score(alltoall100_10gb)
alltoall100_100gb <- calculate_score(alltoall100_100gb)
```

```{r}
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2)$score)
summary(subset(alltoall100_10gb, spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2)$score)

summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2)$unique_score)
summary(subset(alltoall100_10gb, spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2)$unique_score)
```

is it different for reads and writes?

```{r}
#write 
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write")$unique_score)
summary(subset(alltoall100_10gb, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "write")$unique_score)

#read 
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read")$unique_score)
summary(subset(alltoall100_10gb, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")$unique_score)
```

read results seem slightly harder to separate than write results
What are the expected results?

```{r}
#write 
match <- subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "write")
dontmatch <- subset(alltoall100_10gb, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "write")
#threshold of 0.9970 
len_match <- length(match$distance)
len_dontmatch <- length(dontmatch$distance)
threshold = 0.9970
length(subset(match, unique_score >= threshold)$distance)/len_match
length(subset(dontmatch, unique_score >= threshold)$distance)/len_dontmatch
length(subset(match, score >= threshold)$distance)/len_match
length(subset(dontmatch, score >= threshold)$distance)/len_dontmatch
#threshold of 0.9610 
threshold = 0.9610
length(subset(match, unique_score >= threshold)$distance)/len_match
length(subset(dontmatch, unique_score >= threshold)$distance)/len_dontmatch
length(subset(match, score >= threshold)$distance)/len_match
length(subset(dontmatch, score >= threshold)$distance)/len_dontmatch

#read 
match <- subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read")
dontmatch <- subset(alltoall100_10gb, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")
#threshold of 0.9970 
len_match <- length(match$distance)
len_dontmatch <- length(dontmatch$distance)
threshold = 0.9970
length(subset(match, unique_score >= threshold)$distance)/len_match
length(subset(dontmatch, unique_score >= threshold)$distance)/len_dontmatch
length(subset(match, score >= threshold)$distance)/len_match
length(subset(dontmatch, score >= threshold)$distance)/len_dontmatch
#threshold of 0.9610 
threshold = 0.9610
length(subset(match, unique_score >= threshold)$distance)/len_match
length(subset(dontmatch, unique_score >= threshold)$distance)/len_dontmatch
length(subset(match, score >= threshold)$distance)/len_match
length(subset(dontmatch, score >= threshold)$distance)/len_dontmatch
```

### Do these results depend on the access pattern?  (investigation on the infinite value)
 
```{r}
#dont match read 
summary(subset(alltoall100_512mb, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")$unique_score)
summary(subset(alltoall100_512mb, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")$score)

#match read nto1 contig 
summary(subset(alltoall100_512mb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "contig" & filesnb1 == "nto1")$unique_score)
summary(subset(alltoall100_512mb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "contig"& filesnb1 == "nto1")$score)

#match read noncontig 
summary(subset(alltoall100_512mb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "noncontig")$unique_score)
summary(subset(alltoall100_512mb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "noncontig")$score)

#match read nto1 
summary(subset(alltoall100_512mb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nto1")$unique_score)
summary(subset(alltoall100_512mb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nto1")$score)

#match read nton 
summary(subset(alltoall100_512mb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nton")$unique_score)
summary(subset(alltoall100_512mb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nton")$score)
```

 noncontig patterns are hard to match. 
 
```{r}
#dont match read 
summary(subset(alltoall100_10gb, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")$unique_score)
summary(subset(alltoall100_10gb, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")$score)

#match read nto1 contig 
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "contig"& filesnb1 == "nto1")$unique_score)
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "contig"& filesnb1 == "nto1")$score)

#match read noncontig 
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "noncontig")$unique_score)
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "noncontig")$score)

#match read nto1 
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nto1")$unique_score)
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nto1")$score)

#match read nton 
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nton")$unique_score)
summary(subset(alltoall100_10gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nton")$score)
```
 
 we can see a threshold for noncontig now, and we did not lose the good results for nton
 

```{r}
#dont match read 
summary(subset(alltoall100_100gb, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")$unique_score)
summary(subset(alltoall100_100gb, (spatiality1 != spatiality2 | reqsize1 != reqsize2 | procs1 != procs2 | filesnb1 != filesnb2) & operation1 == "read")$score)

#match read nto1 contig 
summary(subset(alltoall100_100gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "contig" & filesnb1 == "nto1")$unique_score)
summary(subset(alltoall100_100gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "contig" & filesnb1 == "nto1")$score)

#match read noncontig 
summary(subset(alltoall100_100gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "noncontig")$unique_score)
summary(subset(alltoall100_100gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & spatiality1 == "noncontig")$score)

#match read nto1 
summary(subset(alltoall100_100gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nto1")$unique_score)
summary(subset(alltoall100_100gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nto1")$score)

#match read nton 
summary(subset(alltoall100_100gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nton")$unique_score)
summary(subset(alltoall100_100gb, spatiality1 == spatiality2 & reqsize1 == reqsize2 & procs1 == procs2 & filesnb1 == filesnb2 & operation1 == "read" & filesnb1 == "nton")$score)
```

it did not change much


 

