#this script reads the .txt files created by the dtw_all.c code to generate the .csv for analysis
import commands

def parse_trace_name(line):
    ret = {}
    lista = line.split('_')
    ret["spatiality"] = lista[2]
    ret["reqsize"] = lista[3]
    ret["procs"] = lista[10]
    ret["filesnb"] = lista[6]
    ret["repetition"] = int(lista[13])
    ret["server"] = int(lista[15].split('.')[0].split('-')[1])
    lista = lista[len(lista)-1].split('.')
    ret["operation"]= lista[len(lista)-1].split('\t')[0]
    if ((ret["spatiality"] != "contig") and (ret["spatiality"] != "noncontig")) or (not (ret["reqsize"] in ["4096reqsX32KB", "32reqsX4096KB", "8reqsX16384KB"])) or (not (ret["procs"] in ["32procs", "64procs"])) or (not (ret["filesnb"] in ["nto1", "nton"])) or (not (ret["operation"] in ["read", "write"])):
        print "PANIC! I dont understand this trace name"
        print line
        return {}
    if ret["server"] == 10:
        ret["server"] = 0
    elif ret["server"] == 15:
        ret["server"] = 1
    elif ret["server"] == 16:
        ret["server"] = 2
    elif ret["server"] == 17:
        ret["server"] = 3
    else:
        print "PANIC! I dont understand this server name"
        print line
        return {}
    return ret

def write_pattern_id(id_csv,pattern,teste):
    id_csv.write(str(pattern)+";"+str(teste["server"])+";"+teste["spatiality"]+";"+teste["reqsize"]+";"+teste["procs"]+";"+teste["filesnb"]+";"+teste["operation"]+";"+str(teste["repetition"])+"\n")


#########################################################################
#get all files
files = commands.getoutput("ls all_comparisons*.txt").split('\n') #1s only
if ("No such" in files[0]) or ("encontrado" in files[0]):
    print "No files?"
    exit()
prefix = "/home/fran/traces_nton/multiapp/" #where the trace files are

#read everything from them
csv = open("all_compare_parsed.csv", "w")
csv.write("server;pattern1;pattern2;score;spatiality1;spatiality2;reqsize1;reqsize2;procs1;procs2;filesnb1;filesnb2;operation1;operation2;repetition1;repetition2\n")
info={}
score = {}
for filename in files:
    print filename
    arq = open(filename, "r")

    #get the number of patterns
    linha = arq.readline()
    pat = int(linha.split(' ')[0])
    pat -= 2 

    for i in range(pat):
        #get the filename so we can know the parameters of the test
	while not (prefix in linha):
	        linha = arq.readline()
        if(linha == ""):
            break
        lista = linha.split(' ')[0].split('/')
        trace = lista[len(lista)-2] + "/" + lista[len(lista)-1]
#        print linha
        pattern = int(linha.split('\t')[1].split('\n')[0])

        #get the parameters of the test
	print trace
        teste = parse_trace_name(trace)
        if not (teste["server"] in score):
            score[teste["server"]] = {}
        if not (teste["server"] in info):
            info[teste["server"]] = {}
        if not (pattern in info[teste["server"]]):
            info[teste["server"]][pattern] = teste

        #read all comparisons of this pattern
        if not (pattern in score[teste["server"]]):
            score[teste["server"]][pattern]={}
        for j in range(i+1, pat-1):
            linha = arq.readline()
            if(linha == ""):
                if "part" in filename:
                    break
                else:
                    print "PANIC! File is not complete"
                    exit()

            pattern2 = int(linha.split('\t')[1])
            this_score = int(linha.split('\n')[0].split('\t')[2])
            if this_score < 0:
                print "PANIC! Negative score"
                exit()
            if not (pattern2 in score[teste["server"]][pattern]):
                score[teste["server"]][pattern][pattern2] = this_score
            elif this_score != score[teste["server"]][pattern][pattern2]:
                print "Diverging scores:",
                print score[teste["server"]][pattern][pattern2],
                print this_score


    arq.close()

#write csv
for server in score:
    for pat1 in score[server]:
        for pat2 in score[server][pat1]:
            if pat1 < pat2:
                csv.write(str(server)+";"+str(pat1)+";"+str(pat2)+";"+str(score[server][pat1][pat2])+";")
                csv.write(info[server][pat1]["spatiality"]+";"+info[server][pat2]["spatiality"]+";"+info[server][pat1]["reqsize"]+";"+info[server][pat2]["reqsize"]+";")
                csv.write(info[server][pat1]["procs"]+";"+info[server][pat2]["procs"]+";"+info[server][pat1]["filesnb"]+";"+info[server][pat2]["filesnb"]+";")
                csv.write(info[server][pat1]["operation"]+";"+info[server][pat2]["operation"]+";"+str(info[server][pat1]["repetition"])+";"+str(info[server][pat2]["repetition"])+"\n")
csv.close()
