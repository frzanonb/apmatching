import subprocess

dirs=subprocess.getoutput("ls | grep _comp | grep 0").split('\n')
remove = []
for elem in dirs:
	if ".csv" in elem:
		remove.append(elem)
for elem in remove:
	dirs.remove(elem)
print(dirs)


csv = open("all_compare.csv", "w")

for dir in dirs:
	print(dir)
	compression = int(dir.split('_')[1].split('p')[1])
	if "_inf" in dir:
		infi = dir.split('_')[2].split('f')[1]
	else:
		infi = "512MB"
	filename = dir+"/all_compare_parsed.csv"
	arq = open(filename, "r")
	for linha in arq:
		if "server" in linha:
			if (dir == dirs[0]):
				csv.write("compression;infinite;"+linha)
		else:
			csv.write(str(compression)+";"+infi+";"+linha)
	arq.close()
csv.close()
