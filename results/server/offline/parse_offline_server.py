import commands
import sys
from numpy import max,mean,median,min

# dir is something like singlepattern_noncontig_8reqsX16384KB_64procs_nto1_write_maxdiff90_threshold70_server1/
# return a dict with parameters
def parse_test_name(dir):
	ret = {}
	lista = dir.split('/')[0].split('_')
	ret["operation"] = lista[1]
	ret["maxdiff"] = int(lista[2].split('f')[2])
	ret["threshold"] = int(lista[3].split('d')[1])
	ret["server"] = int(lista[4].split('r')[2])
	ret["repetition"] = int(lista[5].split('p')[1])
	#sanity check
	if ((ret["operation"] != "write") and (ret["operation"] != "read")):
		print "PANIC! I don't understand this test name...",
		print dir
		exit()
	return ret

#receive the test information obtained with the parse_test_name function and returns a string to be written to the csv 
def get_test_info_str(test,compression):
	return str(test["operation"])+";"+str(test["maxdiff"])+";"+str(test["threshold"])+";"+str(test["server"])+";1;"+str(compression)+";"+str(test["repetition"])+";"

#receive a pattern (like the ones generated in the make_tests_list function) and gives back the string to be written to the csv about it (files;contig;reqsize)
def get_pattern_str(id):
	parsed = id.split('_')
	return parsed[4]+";"+parsed[0]+";"+parsed[1]+";"+parsed[2]

#returns the sum of a list of numbers:
def sum_of_list(lista):
	soma = 0
	for elem in lista:
		soma += elem
	return soma

# line is something like "AGIOS: apply_DTW(): Compared with DTW to a pattern of 10 requests, total amount of accessed data 962560.00, 0 reads (0.00) and 940 writes (962560.00) with 1 files. Got score 344019, which is translated to 99 (max is 1742214033)"
#return the dtw-calculated distance
def get_distance(line):
	parsed = line.split()
	return int(parsed[29].split(',')[0])

#line is something like AGIOS: PATTERN_MATCHING_select_next_algorithm(): We did not find a match, stored a new pattern with id 0
# return the id
def get_new_id(line):
	lista = line.split('\n')[0].split(' ')
	return int(lista[len(lista)-1])

#line is something like AGIOS: write_pattern_matching_file(): We'll write the pattern_matching file. We have 3 patterns (from 3 calls in this execution, average size 682), max DTW score is 897397. We've called FastDTW 3 times to patterns of average size 927
# return a dict with information
def get_final_information(line):
	ret = {}
	lista = line.split('\n')[0].split(' ')
	ret["known_patterns"] = int(lista[9])
	ret["pm_call_count"] = int(lista[12])
	ret["max_dtw_score"] = int(lista[24].split('.')[0])
	ret["fastdtw_call_count"] = int(lista[28])
	ret["avg_call_size"] = int(lista[len(lista)-1])
	return ret

#each position of lista is a list of known ids for the same pattern
def get_avg_repetition(lista, dir, linenb):
	sum = 0
	nb = 0
	visited=[]
	for elem in lista:
		lista2 = []
		for i in range(len(elem)):
			if not (elem[i] in visited):
				visited.append(elem[i])
				lista2.append(i)
		if len(lista2) > 0:
			sum += len(lista2)
			nb += 1
	if nb == 0:
		return 0
	else:
		return [sum/nb,nb]

# line is something like "AGIOS: PATTERN_MATCHING_select_next_algorithm(): Found a match with id 9
# return id
def get_matched_id(line):
	lista = line.split('\n')[0].split(' ')
	ret = int(lista[len(lista)-1])
	return ret

#line is something like "AGIOS: PATTERN_MATCHING_init(): Running PATTERN MATCHING with minimum pattern size 5, threshold 60, and maximum difference 90"
# return minimum pattern size
def get_min_pattern_size(line):
	lista = line.split('\n')[0].split(',')[0].split(' ')
	return int(lista[len(lista)-1])

# line is something like "AGIOS: PATTERN_MATCHING_select_next_algorithm(): Let's compare this pattern to the one we've predicted (3). Are they the same? 0. Are they compatible? 1. DTW score: 41757696  (because max is 4536737)"
# patlen is a dict that has the length (in number of requests) of all ids, reqnb is the length of the current pattern, threshold is the threshold used for this test
# return a dict
def get_prediction_info(line, patlen, reqnb, threshold):
	ret = {}
	lista = line.split('\n')[0].split('.')
	lista0 = lista[0].split('(')
	ret["predicted_id"] = int(lista0[len(lista0)-1].split(')')[0])
	ret["same"] = int(lista[1].split('?')[1].split(' ')[1])
	#the line talks about compatible, but that means simply that the patterns are similar enough (according to the maxdiff parameter that we should apply FastDTW to compare them. We need to see the fastDTW result to say if they are compatible (in the context of this analysis, where compatible means they should be the same) or not. We don't use the function that does that in the code because it would mess with max dtw score, and that would mean the printf would change the code behavior, which is not acceptable.
	if ret["same"] == 1:
		ret["compatible"] = 1
	else:
		compat = int(lista[2].split('?')[1].split(' ')[1])
		if compat == 1:
			lista = lista[len(lista)-1].split(' ')
			dtw_score = int(lista[3])
			max_dtw = int(lista[8].split(')')[0])
			dtw_score = dtw_score/min(reqnb,patlen[ret["predicted_id"]])
			if dtw_score > max_dtw:
				dtw_score = 0
			elif dtw_score == 0:
				dtw_score = 100
			else:
				dtw_score = ((max_dtw - dtw_score)*100)/max_dtw
			if dtw_score >= threshold:
				ret["compatible"]=1
			else:
				ret["compatible"]=0
		else:
			ret["compatible"] = 0
	return ret

#line is something like "AGIOS: PATTERN_MATCHING_select_next_algorithm(): Adding a pattern of 11416 requests, total amount of accessed data 365312.0000, 11416 reads (365312.00) and 0 writes (0.00) with 1 files"
# return the number of requests in the new pattern
def get_pattern_reqnb(line):
	return int(line.split(' ')[6])
	
#print a line of the csv file that contains information about one execution
def print_execution_information(arq, test, execution, avg_repetitions, true_negative, false_negative, true_positive, false_positive, final_info, real_pattern_count, min_pattern_size, ignored_count, predictions, right_predictions, wrong_predictions, compatible_predictions,trace_id):
	#information about test and execution
	arq.write(test["operation"]+";"+str(test["maxdiff"])+";"+str(test["threshold"])+";"+str(min_pattern_size)+";"+str(test["server"])+";"+str(execution)+";"+trace_id+";")
	#metrics
	arq.write(str(avg_repetitions)+";"+str(true_negative)+";"+str(false_negative)+";"+str(true_positive)+";"+str(false_positive)+";"+str(ignored_count)+";")
	#final information
	arq.write(str(final_info["known_patterns"])+";"+str(final_info["pm_call_count"])+";"+str(final_info["max_dtw_score"])+";"+str(final_info["fastdtw_call_count"])+";"+str(final_info["avg_call_size"])+";"+str(real_pattern_count)+";")
	#predictions
	arq.write(str(predictions)+";"+str(right_predictions)+";"+str(wrong_predictions)+";"+str(compatible_predictions)+"\n")
	
#list (making a list, literally) the existing access patterns we can find in the traces (hardcoded information)
def make_tests_list():
	ret = []
	for s in ["contig", "noncontig"]:
		for r in ["4096reqsX32KB", "32reqsX4096KB", "8reqsX16384KB"]:
			for p in ["32procs", "64procs"]:
				for f in ["nto1", "nton"]:
					if (s == "contig") and (r == "4096reqsX32KB"):
						continue
					if (s == "noncontig") and (f == "nton"):
						continue
					ret.append({"spatiality": s, "reqsize": r, "procs": p, "filesnb": f})
	return ret

# p is a pattern, an item from the list returned by make_tests_list()
# returns a string that is part of the name of the trace files corresponding to this test
def make_test_id(p):
	if len(p) == 0:
		return ""
	ret = p["spatiality"]+"_"+p["reqsize"]+"_"+p["procs"]+"_32machines_"+p["filesnb"]+"_"
	return ret

#line is something like "/home/fboito/fs_suite/traces/agios_SMALLER_evaluation_NOOP_app1_noncontig_32reqsX4096KB_64procs_32machines_nto1_6_pvfstrace/pvfstrace_parasilo-10.rennes.grid5000.fr.read
#return a dict in the format created by make_tests_list (so we can give to make_test_id)
def parse_trace_name(line):
	ret = {}
	lista = line.split('/')
	lista = lista[len(lista)-2].split('_')	
	ret["spatiality"] = lista[2]
	ret["reqsize"] = lista[3]
	ret["procs"] = lista[10]
	ret["filesnb"] = lista[6]
	if ((ret["spatiality"] != "contig") and (ret["spatiality"] != "noncontig")) or (not (ret["reqsize"] in ["4096reqsX32KB", "32reqsX4096KB", "8reqsX16384KB"])) or (not (ret["procs"] in ["32procs", "64procs"])) or (not (ret["filesnb"] in ["nto1", "nton"])):
		print "PANIC! I dont understand this trace name"
		print line
		return {}
	return ret

############################################################################################
#take the list of directories
root = sys.argv[1]
tests = commands.getoutput("ls "+root+"/ |grep multiplepatternsmixed_").split('\n')
if ("No such" in tests[0]) or ("encontrado" in tests[0]):
	print "PANIC! No files?"
	exit()
#remove scripts
rem = []
for elem in tests:
	if (".py" in elem) or (".sh" in elem) or (".csv" in elem):
		rem.append(elem)
for elem in rem:
	tests.remove(elem)

#we'll write results to a csv file
contexts=["all", "strided", "contig", "nto1", "nton"]
metrics=["true_negative", "false_negative", "true_positive", "false_positive"]
csv = open("server_multiapp_evaluation.csv", "w")
csv.write("operation;maxdiff;threshold;server;window_size;compression;repetition;")
for context in contexts:
	for metric in metrics:
		csv.write(metric+"_"+context+";")
csv.write("known_pat;fastdtw_calls;fastdtw_size\n")
#and we'll also create a file with the detected access patterns (so we can translate into performance improvements)
detections = open("server_multiapp_detectedap.csv", "w")
detections.write("operation;maxdiff;threshold;server;window_size;compression;repetition;files;contig;reqsize;proc;files_det;contig_det;reqsize_det;proc_det\n")

prefix = "/home/fran" #prefix used in the multiplepatterns_ script, without the /
compression = 100

#make a list of possible access patterns
existing_tests = make_tests_list()

#go through them
debug = 0
max_distance_good = []
max_distance_bad = []
for dir in tests:
	print dir
	#parse the directory name for test parameters
	test = {}
	test = parse_test_name(dir)
	#read  the test trace
	arq = open(root+"/"+dir+"/test_trace.txt", "r")
	execution = 0
	linenb = 0
	pattern_list = {} #this stores the known ids of expected patterns, we'll separate inside by test
	for p in existing_tests:
		id = make_test_id(p) 
		pattern_list[id] = []
	idtopattern = {} #this stores the mapping from ids (from the pattern matching mechanism) to ids from this script, which are string representations of the access patterns (files;contig;reqsize)
	stats = {}
	cumulative_stats = {}
	for context in contexts:
		stats[context]={}
		cumulative_stats[context] = {}
		for metric in metrics:
			stats[context][metric] = 0
			cumulative_stats[context][metric] = []
	cumulative_known_pat = []
	cumulative_fastdtw_calls = []
	cumulative_fastdtw_size = []
	id = ""
	detections_str = ""
	this_max_dist_good = 0
	this_max_dist_bad = 0
	last_dist = 0
	for linha in arq:
		linenb+=1
		if "NOOP_app1_" in linha: #line with the name of the trace, we'll get the identifier from it
			id = make_test_id(parse_trace_name(linha))
			if id == "":
				arq.close()
				csv.close()
				exit()
			if debug == 1:
				print "in the new execution trace is",
				print id
		if "/dtw_test "+prefix+"/trace.txt" in linha: #new execution
			execution += 1	
			#reset counters	
			pattern=-1
			for context in contexts:
				for metric in metrics:
					stats[context][metric] = 0
			ignored_count = 0
			predictions=0
			right_predictions = 0
			wrong_predictions = 0
			compatible_predictions = 0
			if debug == 1:
				print "execution",
				print execution
		elif "Could not open pattern matching file" in linha: #this is not supposed to happen after the first execution
			if execution > 1:
				print dir
				print "line ",
				print linenb
				print "Something went wrong."
				arq.close()
				csv.close()
				exit()
		elif "patterns from the pattern matching file, the maximum DTW score so far" in linha: #this is not supposed to happen in the first execution
			if execution == 1:
				print dir
				print "line ",
				print linenb
				print "Something went wrong."
				arq.close()
				csv.close()
				exit()
		elif "PATTERN_MATCHING_select_next_algorithm(): Adding a pattern" in linha: #we have a pattern, will try to match it
			pattern+=1
			if debug == 1:
				print "new pattern, current is number",
				print pattern,
		elif "AGIOS: PATTERN_MATCHING_select_next_algorithm(): We'll ignore" in linha:
			#this pattern was not a pattern
			ignored_count+=1
			#undo the new pattern part
			pattern-=1
			if debug == 1:
				print "ignoring a pattern, rollback to",
				print pattern,
		elif "We did not find a match, stored a new pattern" in linha: #new id for the same pattern
			new_id = get_new_id(linha)
			pattern_list[id].append(new_id) #this is now a known id for this pattern
			#if we do not have a match, it could be a false or a true negative
			if len(pattern_list[id]) == 1: #this was the first time this pattern was seen, it was not supposed to match
				stats["all"]["true_negative"]+=1
				if "noncontig" in id:
					stats["strided"]["true_negative"]+=1
				elif "contig" in id:
					stats["contig"]["true_negative"]+=1
				else:
					print "I dont understand this pattern???",
					print id
					exit()
				if "nto1" in id:
					stats["nto1"]["true_negative"] += 1
				elif "nton" in id:
					stats["nton"]["true_negative"]+=1 
				else:
					print "I dont understand this pattern???",
					print id
					exit()
			else: #we have failed to match 
				stats["all"]["false_negative"]+=1
				if "noncontig" in id:
					stats["strided"]["false_negative"]+=1
				elif "contig" in id:
					stats["contig"]["false_negative"]+=1
				else:
					print "I dont understand this pattern???",
					print id
					exit()
				if "nto1" in id:
					stats["nto1"]["false_negative"] += 1
				elif "nton" in id:
					stats["nton"]["false_negative"]+=1 
				else:
					print "I dont understand this pattern???",
					print id
					exit()
			detections_str += get_test_info_str(test,compression) + get_pattern_str(id) + ";-;-;-\n"
			assert not (new_id in idtopattern)
			idtopattern[new_id] = id
			if debug == 1:
				print "no match, negatives - true",
				print stats["all"]["true_negative"],
				print "false",
				print stats["all"]["false_negative"]	
		elif "AGIOS: PATTERN_MATCHING_select_next_algorithm(): Found a match with id" in linha: #we have a match
			new_id = get_matched_id(linha)
			#if we have a match, it could be a true or a false positive
			if new_id in pattern_list[id]: #match with a known id for the same pattern
				stats["all"]["true_positive"]+=1
				if "noncontig" in id:
					stats["strided"]["true_positive"]+=1
				elif "contig" in id:
					stats["contig"]["true_positive"]+=1
				else:
					print "I dont understand this pattern???",
					print id
					exit()
				if "nto1" in id:
					stats["nto1"]["true_positive"] += 1
				elif "nton" in id:
					stats["nton"]["true_positive"]+=1 
				else:
					print "I dont understand this pattern???",
					print id
					exit()
				if last_dist > this_max_dist_good:
					this_max_dist_good = last_dist
			else: #match with something else, should not be a match
				stats["all"]["false_positive"]+=1
				if "noncontig" in id:
					stats["strided"]["false_positive"]+=1
				elif "contig" in id:
					stats["contig"]["false_positive"]+=1
				else:
					print "I dont understand this pattern???",
					print id
					exit()
				if "nto1" in id:
					stats["nto1"]["false_positive"] += 1
				elif "nton" in id:
					stats["nton"]["false_positive"]+=1 
				else:
					print "I dont understand this pattern???",
					print id
					exit()
				if last_dist > this_max_dist_bad:
					this_max_dist_bad = last_dist
			assert new_id in idtopattern
			detections_str += get_test_info_str(test,compression) + get_pattern_str(id) + ";" + get_pattern_str(idtopattern[new_id])+"\n"
			if debug == 1:
				print "match with",
				print new_id,
				print "positives - true",
				print stats["all"]["true_positive"],
				print "false",
				print stats["all"]["false_positive"]
		elif "AGIOS: write_pattern_matching_file(): We'll write" in linha: #finishing this execution, get information from it
			final_info = get_final_information(linha)
			for context in contexts:
				for metric in metrics: 
					cumulative_stats[context][metric].append(stats[context][metric])
			cumulative_known_pat.append(final_info["known_patterns"])
			cumulative_fastdtw_calls.append(final_info["fastdtw_call_count"])
			cumulative_fastdtw_size.append(final_info["avg_call_size"])
			if debug == 1:
				print "ending this execution\n---------------------------------------------------------------------------------"	
		elif "files. Got score" in linha:
			last_dist = get_distance(linha)
		else:
			continue
	arq.close()
	#now we can write the cumulative metrics from this test to the csv file
	csv.write(get_test_info_str(test,compression)) #write "ionodes_nb;operation;maxdiff;threshold;ionode;window_size;compression;"	
	for context in contexts:
		for metric in metrics:
			csv.write(str(sum_of_list(cumulative_stats[context][metric]))+";")
	csv.write(str(max(cumulative_known_pat))+";"+str(sum_of_list(cumulative_fastdtw_calls))+";"+str(mean(cumulative_fastdtw_size))+"\n")
	max_distance_good.append(this_max_dist_good)
	max_distance_bad.append(this_max_dist_bad)
	
csv.close()
detections.close()
print "good matches"
print min(max_distance_good)
print median(max_distance_good)
print max(max_distance_good)
print "bad matches"
print min(max_distance_bad)
print median(max_distance_bad)
print max(max_distance_bad)
