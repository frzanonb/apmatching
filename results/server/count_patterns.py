import subprocess

class OutputFile():
	def __init__(self, filename, header):
		self.arq = open(filename+".csv", "w")
		self.arq.write(header)
		self.buf = ""
		self.buflimit = 1024*1024
	def write(self, values):
		for i in range(len(values)):
			self.buf += values[i]
			if i < (len(values)-1):
				self.buf += ";"
		self.buf += "\n"
		if len(self.buf) > self.buflimit:
			self.arq.write(self.buf)
			self.buf = ""
	def close(self):
		self.arq.write(self.buf)
		self.arq.close()

def get_reqsize(line):
	return str(int(line.split('X')[1].split('K')[0]))

servernb = 4
ops = ["read", "write"]
compression = [10, 100, 1000]
servers={}
servers[10] = 1
servers[15] = 2
servers[16] = 3
servers[17] = 4
prefix = "/home/fran/traces_nton/multiapp/" #where the trace files are


#create the output file
csv = OutputFile("pattern_count", "operation;server;compression;repetition;proc2;files1;files2;contig1;contig2;reqsize1;reqsize2;pat_number\n")

for op in ops:
	for comp in compression:
		if op == "read" and comp == 10:
			continue
		if op == "write" and comp != 10:
			continue
		for i in range(1, servernb+1):
			patnb={}
			filename = "all_comparisons_"+op+"_1s_comp"+str(comp)+"_server"+str(i)+".txt"
			print(filename)
			arq = subprocess.getoutput("cat "+filename+" | grep "+prefix).split('\n')
			for line in arq:
				#get access pattern from line
				parsed = line.split('/')[5].split('_')
				contig = [parsed[2], parsed[8]]
				reqsize = [get_reqsize(parsed[3]), get_reqsize(parsed[9])]
				procs = str(int(parsed[10].split('p')[0]))
				nto = [parsed[6], parsed[12]]
				repetition = str(int(parsed[13]))
				server = servers[int(line.split('/')[6].split('.')[0].split('-')[1])]
				if(server != i):
					print("we are looking at results for server "+str(i)+", but this line has server "+str(server))
					print(line)
					exit()
				parsed = line.split('.')
				operation = parsed[len(parsed)-1].split()[0]
				if operation != op:
					print("we are looking at "+op+" results, but this line has operation "+operation)
					print(line)
					exit()
				label = line.split()[0]
				if label in patnb:
					patnb[label]+= 1
				else:
					#if it is the first pattern of a file, we already finished the previous file
					if len(patnb) > 1:
						print("how come we have more than one label?")
						print(patnb)
						exit()
					for other in patnb:
						csv.write([op, str(i), str(comp), repetition, procs, nto[0], nto[1], contig[0], contig[1], reqsize[0], reqsize[1], str(patnb[other])])
					patnb={}
					patnb[label]=1
			for other in patnb:
				csv.write([op, str(i), str(comp), repetition, procs, nto[0], nto[1], contig[0], contig[1], reqsize[0], reqsize[1], str(patnb[other])])

csv.close()
					


