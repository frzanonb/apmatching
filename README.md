Companion repository for the "**On server-side file access pattern matching**" paper, 

by Francieli Zanon Boito, Ramon Nou, Laércio Lima Pilla, Jean Luca Bez, Jean-François Méhaut, Toni Cortes, and Philippe O.A. Navaux


* Univ. Grenoble Alpes, Inria, CNRS, Grenoble INP, LIG, 38000 Grenoble, France
* Barcelona Supercomputing Center, Spain
* LRI, Univ. Paris-Sud – CNRS, Orsay, France
* Institute of Informatics — Federal University of Rio Grande do Sul, Porto Alegre, Brazil
* Universitat Politècnica de Catalunya, Spain


# Organization of this repository


*  The [data/ folder](#the-data-sets) contains the two data sets generated for this research, and a third one;
*  the [pattern\_matching/ folder](#the-source-code) contains the source code of the pattern matching approach and the offline evaluation code;
*  the [results/ folder](#results) contains results generated for the paper.

Due to the large volume of data, many files and folders were compressed before being added to the repository. To decompress file.tgz, use this command:

`tar xzf file.tgz`

# The data sets

The data sets were moved to Zenodo: [http://doi.org/10.5281/zenodo.3340631](http://doi.org/10.5281/zenodo.3340631). They can be freely obtained by following the link. 

# The  source code

The pattern\_matching/ folder contains source code adapted from an original implementation of the mechanism made into the AGIOS I/O scheduling library. It has been adapted to run as a standalone offline evaluation system for this research.

* The pattern\_tracker is used to build patterns of requests. Before usage, a call to pattern\_tracker\_init() must be made. Requests are added by a call to add\_request\_to\_pattern() function, and a mutex keeps the integrity of the internal structures. The get\_current\_pattern() function has to be called periodically to complete and return the built pattern.
* The pattern\_matching is the core of the pattern matching approach. A call to pattern\_matching\_init() sets values for threshold, maxdiff, and minimum\_size (patterns shorter than minimum\_size are ignored by the mechanism); calls the pattern\_tracker\_init() function to initialize the pattern\_tracker; and reads the file containing the knowledge base. The pattern\_matching\_select\_next\_algorithm() function calls get\_current\_pattern() from the pattern\_tracker and keeps the knowledge base (its name comes from the usage inside AGIOS). It also maintains a probability network to predict future patterns, but this part was not studied in depth yet. At the end, a call to pattern\_matching\_init MUST be made to write the state of the knowledge base to its file.
* mylist is a chained list implementation leveraged from the AGIOS library.
* DTW is the FastDTW implementation.
* dtw\_test\_server.c and dtw\_test\_ionodes.c are used to feed a trace to the pattern matching approach. They call the pattern\_matching\_init() functions, then read requests from an input file and add them with add\_request\_to\_pattern(). They make periodic calls to pattern\_matching\_select\_next\_algorithm() to trigger the pattern matching mechanism, which will output debug messages with details of the decisions made.
* dtw\_all\_server.c and dtw\_all\_ionodes.c use the pattern\_tracker directly to read patterns from a list of input files to a list, and then run the all-to-all comparisons. It creates a file with all results, and also outputs the maximum observed distance.
* dtw\_performance.c creates two random patterns of a given length and applies FastDTW to them. It is used to analyze the performance of comparing patterns.
* Makefile compiles all C code.
* In the test\_execution/ folder, multiplepatterns\_ python scripts are used to give a set of traces in a random order to the pattern matching mechanism (using the dtw\_test\_ codes). The generate\_tests\_ scripts make a list of multiplepatterns\_ calls to be made depending on some hardcoded parameters (different values of maxdiff and threshold to be tested, for instance).
* In the test\_execution/ folder, run\_all\_compare\_ python scripts are used to call the dtw\_all tests for the different scenarios.

Some important values:

* in the pattern\_matching/pattern\_tracker.h file, MAX\_PATTERN\_OFFSET is the infinite value (in KB);
* in the pattern\_matching/pattern\_tracker.c file, agios\_config\_pattern\_compression\_factor is the compression factor;
* in the pattern\_matching/pattern\_matching.c file, config\_pattern\_filename is the name (with path) of the file written by the pattern matching mechanism at the end of its execution;
* in the pattern\_matching/pattern\_matching.c file, max\_dtw\_result is the maximum observed distance, which is learnt during execution if update\_max\_dtw\_result is set to 1. Otherwise, it receives a hardcoded value in the pattern\_matching\_init() function.
* In the pattern\_matching/dtw\_test\_server.c, dtw\_test\_ionodes.c, dtw\_all\_server.c, and dtw\_all\_ionodes.c files, select\_algorithm\_period gives the pattern length in seconds;
* The python scripts were not intended for generic usage, hence they have some hardcoded parameters.

# Results

This results/ folder has two parts: [server/](#results-with-data-server-traces) and [ionodes/](#results-with-io-node-traces)

## Results with data server traces

* The results/results\_performance\_fastdtw.csv file has some performance measurements obtained with the dtw\_performance code in a node from the Rennes cluster. This data is analyzed in the fastdtw\_performance.Rmd Rmarkdown file, and exported to the fastdtw\_performance.html file.
* The results/server/pattern\_count.csv file has the number of patterns per benchmark, obtained with the count\_patterns.py script.
* Inside the results/server/all-to-all folder, the output/ folder contains the dtw\_all_ output files. To each folder, the parse\_all\_comparisons.py script is used to generate a .csv file, and the combine\_info.py combine them into the all\_compare.csv file. Here they are already parsed, and the all\_compare.csv contains all results.
* results/server/all-to-all/analysis\_alltoall\_server.Rmd contains the R code used to analyze the all-to-all comparison results, **including results not shown in the paper**. The Rmarkdown file has been exported to analysis\_alltoall\_server.html.
* The results/server/offline/ folder contains results from the offline evaluation. Each results\_server\_ folder contains the output of the multiplepatterns\_ executions, one sub-folder per execution, named accordingly. Inside the output folder of each multiplepatterns\_ execution, the test\_trace.txt text file has all debug messages written during the execution (including by the pattern matching mechanism), and the agios\_pattern\_matching.dat binary file is the file generated by the pattern\_matching at the end of the last call to dtw\_test (i.e. the knowledge base generated by that order of traces with those parameters). The parse\_offline\_server.py script is to be used to parse each folder of results to estimate the number of true and false negatives and positives. The script generates two files: server\_multiapp\_evaluation.csv with these results and a second file called server\_multiapp\_detectedap.csv, which contains to each pattern information about the match made (if a match was found or not, and if it was found, what benchmark the matched pattern represents). The second file is used to estimate the performance results. The .csv files are included in the repository in the results/server/offline/csv folder. As the name suggests, the parse\_offline\_server\_method2.py does the parsing using the method 2 described in the paper (without differentiating between 64+32 and 64+64 processes). This script does not generate the \_detectedap.csv file.
* results/server/offline/offlineanalysis\_server.Rmd contains the R code used to analyze the results of the offline evaluation, **including results not shown in the paper**. The Rmarkdown file has been exported to a .html version, also in the repository.
* The rennes\_orangefs\_agios\_performance.csv contains performance measured with the same methodology as used to obtain the trace files, using different scheduling policies. The code used to estimate performance improvements by the pattern matching approach is in the patternmatching\_analysis\_server.Rmd file, together with all results from the offline analysis and code used to generate the graphs from the paper.

## Results with IO node traces

* The results/ionodes/pattern\_count.csv file has the number of patterns per benchmark, obtained with the count\_patterns.py script.
* Inside the results/ionodes/all-to-all/output folder are the output of the dtw\_all tests. The get\_score\_distributions.py script was applied separately to each compression factor to generate the distance\_ csv files present in the results/ionodes/all-to-all/no\_compression, compression\_10, and compression\_100 folders.
* The results/ionodes/all-to-all/analysis\_alltoall\_ionodes.Rmd file contains the R code used to analyze the all-to-all comparison results, **including results not shown in the paper**. The Rmarkdown file has been exported to a .html, also available in the repository.
* The results/ionodes/offline/ folder contains results from the offline evaluation. Each results\_ionodes\_ folder (inside results/ionodes/offline/all\_results/) contains the output of the multiplepatterns\_ executions, one sub-folder per execution, named accordingly. Inside the output folder of each multiplepatterns\_ execution, the test\_trace.txt text file has all debug messages written during the execution (including by the pattern matching mechanism), and the agios\_pattern\_matching.dat binary file is the file generated by the pattern\_matching at the end of the last call to dtw\_test (i.e. the knowledge base generated by that order of traces with those parameters). The parse\_offline\_ionodes.py script is to be used to parse each folder of results to estimate the number of true and false negatives and positives. The script generates two files: ionodes\_multiapp\_evaluation.csv with these results and a second file called ionodes\_multiapp\_detectedap.csv, which contains to each pattern information about the match made (if a match was found or not, and if it was found, what benchmark the matched pattern represents). The second file is used to estimate the performance results. The .csv files are included in the repository in the csv folder. The ionodes\_detectedap\_plot.csv contains the per pattern information only for the threshold and maxdiff combinations used for the performance analysis.
* results/ionodes/offline/offlineanalysis\_ionodes.Rmd contains all R code used to analyze the offline evaluation results, **including results not shown in the paper**. The Rmarkdown file has been exported to a .html file, also in the repository.


